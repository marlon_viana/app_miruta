package com.example.marlon.miruta.ModulRuta;
import com.example.marlon.miruta.ModulRuta.event.RutasEvent;
import com.example.marlon.miruta.ModulRuta.ui.RutasView;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class RutasPressenterImpl implements RutasPressenter {

    private EventBus eventBus;
    private RutasView view;
    private RutasInteractor interactor;

    public RutasPressenterImpl(EventBus eventBus, RutasView view, RutasInteractor interactor) {
        this.eventBus = eventBus;
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
        interactor.obtenerRutas();
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Subscribe
    @Override
    public void onEventMainThread(RutasEvent event) {
        switch (event.getEventType()){

            case RutasEvent.showMessageRuta:
                view.showMessageLayout(event.getMessage());
                break;

            case RutasEvent.showLayoutRuta:
                view.showLayoutRuta(event.getaBoolean());
                break;

            case  RutasEvent.showRecyclerView:
                view.showRecyclerView(event.getList());
                break;

            case  RutasEvent.showMessage:
                view.showMessage(event.getMessage());
                break;
        }
    }

    @Override
    public void borrarRuta(String _idRuta) {
        interactor.borrarRuta(_idRuta);
    }
}
