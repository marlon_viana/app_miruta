package com.example.marlon.miruta.ModulServicio.di;

import com.example.marlon.miruta.ModulServicio.iu.ServicioActivity;
import com.example.marlon.miruta.Services.ContextModule;
import com.example.marlon.miruta.Services.PreferencesModule;
import com.example.marlon.miruta.Services.ServiceRequestModule;
import com.example.marlon.miruta.lib.di.LibsModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {LibsModule.class, ServicioModule.class, ContextModule.class,ServiceRequestModule.class, PreferencesModule.class})
public interface ServicioComponent {
    void inject(ServicioActivity activity);
}
