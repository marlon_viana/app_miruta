package com.example.marlon.miruta.lib.di;


import com.example.marlon.miruta.lib.GreenRobotEventBus;
import com.example.marlon.miruta.lib.base.EventBus;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

@Module
public class LibsModule {

    public LibsModule() {
    }

    @Provides
    @Singleton
    EventBus providesEventBus(org.greenrobot.eventbus.EventBus eventBus){
        return new GreenRobotEventBus(eventBus);
    }

    @Provides
    @Singleton
    org.greenrobot.eventbus.EventBus providesLibraryEventBus(){
        return new org.greenrobot.eventbus.EventBus().getDefault();
    }

}
