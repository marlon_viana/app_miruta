package com.example.marlon.miruta.ModulServicio.iu;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.marlon.miruta.CustomView.CustomScroll;
import com.example.marlon.miruta.DataModel.Comentario;
import com.example.marlon.miruta.DataModel.DetalleLugar;
import com.example.marlon.miruta.DataModel.LugarTuristico;
import com.example.marlon.miruta.DataModel.Ruta;
import com.example.marlon.miruta.MobileApplication;
import com.example.marlon.miruta.ModulLugar.ui.AdaptadorServicioListener;
import com.example.marlon.miruta.ModulServicio.ServicioPressenter;
import com.example.marlon.miruta.ModulServicio.di.ServicioComponent;
import com.example.marlon.miruta.R;
import com.google.gson.Gson;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.util.List;

import javax.inject.Inject;

public class ServicioActivity extends AppCompatActivity implements ServicioView,View.OnClickListener,
        AdaptadorComentarioListener{

    private Ruta ruta;
    private DetalleLugar detalleLugar;
    private LugarTuristico lugarTuristico;
    private TextView textViewTitle,textViewDescripcion,titeltoolbar;
    private EditText editTextComentario;
    private Button buttonEnviar;
    private RatingBar ratingBarUser,ratingBarGeneral;
    private ConstraintLayout constraintLayoutServicio;
    private RecyclerView recyclerViewComentarios;
    private CustomScroll scrollViewMaster;
    private String idusuario;

    @Inject
    ServicioPressenter pressenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicio);
        getBundle();
        addView();
        addToolBar();
        setupInject();
        pressenter.onCreate();
        pressenter.getValoracionComentarios(detalleLugar.getIDDETALLELTS(),ruta.getIDRUTA());
    }

    private void setupInject() {
        MobileApplication application = (MobileApplication) getApplication();
        ServicioComponent component = application.getServicioComponent(this);
        component.inject(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public void getBundle() {
        String rutaBundle= getIntent().getExtras().getString("RutaBundle");
        String lugarBlundle= getIntent().getExtras().getString("LugarBundle");
        String detalleBundle= getIntent().getExtras().getString("ServicioBundle");
        Gson gson= new Gson();
        ruta= gson.fromJson(rutaBundle,Ruta.class);
        lugarTuristico= gson.fromJson(lugarBlundle,LugarTuristico.class);
        detalleLugar= gson.fromJson(detalleBundle,DetalleLugar.class);
    }

    private void addView() {
        constraintLayoutServicio= findViewById(R.id.constraintLayoutServicio);
        titeltoolbar= findViewById(R.id.textview_titulotoolbar);

        scrollViewMaster= findViewById(R.id.scrollView_master);

        KeyboardVisibilityEvent.setEventListener(
                this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        if (isOpen){
                            scrollViewMaster.setEnableScrolling(false);
                        }else {
                            scrollViewMaster.setEnableScrolling(true);
                        }
                    }
                });

        recyclerViewComentarios= findViewById(R.id.recyclerViewComentarios);

        textViewTitle= findViewById(R.id.text_tituloServicio);
        textViewTitle.setText(detalleLugar.getDLTSNOMBRE());

        textViewDescripcion= findViewById(R.id.textview_descripcionServicio);
        textViewDescripcion.setText(detalleLugar.getDLTSDETALLE());

        editTextComentario= findViewById(R.id.editText_comentario);

        ratingBarUser= findViewById(R.id.califacionEstrella);
        ratingBarGeneral=findViewById(R.id.califacionGeneral);
        buttonEnviar=findViewById(R.id.buttonEnviar);
        buttonEnviar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.buttonEnviar:
                enviarFeedBack();
                break;
        }
    }

    private void addToolBar() {
        Toolbar toolbar=findViewById(R.id.toolbar_servicio);
        toolbar.setTitle("");
        titeltoolbar.setText(R.string.servicio);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void showSnack(String _message) {
        Snackbar mSnackBar = Snackbar.make(constraintLayoutServicio, _message, Snackbar.LENGTH_LONG);
        TextView mainTextView =(mSnackBar.getView()).findViewById(android.support.design.R.id.snackbar_text);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            mainTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }else{
            mainTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        mainTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        mainTextView.setTextColor(getResources().getColor(R.color.colorblanco));
        mSnackBar.show();
    }

    public void showMessageOption(String _message, final String selection) {
        Snackbar snackbar=Snackbar.make(constraintLayoutServicio,_message, Snackbar.LENGTH_LONG);
        TextView mainTextView =(snackbar.getView()).findViewById(android.support.design.R.id.snackbar_text);
        snackbar.setAction(R.string.si, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pressenter.borrarComentario(selection);
            }
        }).setActionTextColor(getResources().getColor(R.color.colorPrimary));
        mainTextView.setTextColor(getResources().getColor(R.color.colorblanco));
        snackbar.show();
    }

    @Override
    public void showComentarios(List _list) {

        recyclerViewComentarios.setVisibility(View.VISIBLE);
        recyclerViewComentarios.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL,false));
        AdaptadorComentario adapter= new AdaptadorComentario(_list,idusuario,this);
        recyclerViewComentarios.setAdapter(adapter);

        LayoutAnimationController controller= AnimationUtils.loadLayoutAnimation
                (recyclerViewComentarios.getContext(),R.anim.animlt_lefttoright);
        recyclerViewComentarios.setLayoutAnimation(controller);
        recyclerViewComentarios.getAdapter().notifyDataSetChanged();
        recyclerViewComentarios.scheduleLayoutAnimation();
    }

    @Override
    public void showRaiting(int _rating) {
        ratingBarGeneral.setRating(_rating);
    }

    @Override
    public void getIdUser(String _iduser) {
        idusuario=_iduser;
    }

    private void enviarFeedBack(){
        String comentario= editTextComentario.getText().toString().trim()
                                        .replaceAll("\n", " ");

        int estrella=Math.round(ratingBarUser.getRating()) ;
        String numeroEstrella= String.valueOf(estrella);

        if (!comentario.isEmpty()){
            pressenter.sendComentario(detalleLugar.getIDDETALLELTS(),"Comentario",comentario);

            if (!numeroEstrella.equals("0")||numeroEstrella.isEmpty()){
                pressenter.sendCalificacion(detalleLugar.getIDDETALLELTS(),numeroEstrella,ruta.getIDRUTA());
            }

            editTextComentario.setText("");
            ratingBarUser.setRating(0);

        }else {
            showSnack(getString(R.string.ingresar_comentario));
        }
    }

    @Override
    public void borrarComentario(Comentario _comentario) {
        showMessageOption(getString(R.string.borrar_comentario),_comentario.getIDSERVICIOCOMENTARIO());
    }
}
