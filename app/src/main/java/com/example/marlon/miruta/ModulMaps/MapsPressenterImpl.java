package com.example.marlon.miruta.ModulMaps;
import android.util.Log;

import com.example.marlon.miruta.ModulMaps.event.MapsEvent;
import com.example.marlon.miruta.ModulMaps.iu.MapsView;
import com.example.marlon.miruta.lib.base.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MapsPressenterImpl implements MapsPressenter {

    private EventBus eventBus;
    private MapsView view;
    private MapsInteractor interactor;

    public MapsPressenterImpl(EventBus eventBus, MapsView view, MapsInteractor interactor) {
        this.eventBus= eventBus;
        this.view= view;
        this.interactor= interactor;
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Subscribe
    @Override
    public void onEventMainThread(MapsEvent event) {
        switch (event.getEventType()) {

            case MapsEvent.showMessageInfo:
                view.showMessageLayout(event.getMessage());
                break;

            case MapsEvent.showNodos:
                view.showNodos(event.getList());
                break;

            case MapsEvent.showLayoutMaps:
                view.showLayoutMapa(event.getaBoolean());
                break;

            case MapsEvent.showPoly:
                view.showRutas(event.getList());
                break;

        }
    }

    @Override
    public void getNodos(String _id) {
        interactor.getNodos(_id);
    }
}
