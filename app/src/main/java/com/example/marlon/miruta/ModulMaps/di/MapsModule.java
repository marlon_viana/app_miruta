package com.example.marlon.miruta.ModulMaps.di;

import android.content.Context;
import com.example.marlon.miruta.ModulMaps.MapsInteractor;
import com.example.marlon.miruta.ModulMaps.MapsInteractorImpl;
import com.example.marlon.miruta.ModulMaps.MapsPressenter;
import com.example.marlon.miruta.ModulMaps.MapsPressenterImpl;
import com.example.marlon.miruta.ModulMaps.iu.MapsView;
import com.example.marlon.miruta.Services.PreferencesManager;
import com.example.marlon.miruta.Services.ServiceRequest;
import com.example.marlon.miruta.lib.base.EventBus;

import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;


@Module
public class MapsModule {

    private MapsView view;

    public MapsModule(MapsView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    MapsPressenter providerMapsPressenter(EventBus eventBus, MapsView view, MapsInteractor interactor){
        return new MapsPressenterImpl(eventBus, view, interactor);
    }
    @Provides
    @Singleton
    MapsInteractor providerMapsInteractor(EventBus eventBus, ServiceRequest request, PreferencesManager preferences, Context context){
        return new MapsInteractorImpl(eventBus, request, preferences, context);
    }

    @Provides
    @Singleton
    MapsView providerMainView(){
        return this.view;
    }

}
