package com.example.marlon.miruta.Services;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ServiceRequestModule {
    private String BASE_URL;

    public ServiceRequestModule(String BASE_URL) {
        this.BASE_URL = BASE_URL;
    }

    @Provides
    @Singleton
    ServiceRequest getServiceRequest(Retrofit retrofit){
        return retrofit.create(ServiceRequest.class);
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit(OkHttpClient.Builder httpClient, Retrofit.Builder retrofitBuilder){
        return retrofitBuilder.client(httpClient.build()).build();
    }

    @Provides
    @Singleton
    Retrofit.Builder providesRetrofitBuilder(String baseUrl){
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create());
    }

    @Provides
    @Singleton
    OkHttpClient.Builder providesOkHttpClient(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        return httpClient;
    }

    @Provides
    @Singleton
    String providesBaseUrl(){
        return BASE_URL;
    }
}
