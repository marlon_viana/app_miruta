package com.example.marlon.miruta.ModulLugar;

import android.content.Context;
import android.util.Log;

import com.example.marlon.miruta.DataModel.Message;
import com.example.marlon.miruta.ModulLugar.event.LugarEvent;
import com.example.marlon.miruta.R;
import com.example.marlon.miruta.Services.PreferencesManager;
import com.example.marlon.miruta.Services.ServiceConectivity;
import com.example.marlon.miruta.Services.ServiceRequest;
import com.example.marlon.miruta.lib.base.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LugarInteractorImpl implements LugarInteractor {
    private EventBus eventBus;
    private ServiceRequest request;
    private PreferencesManager preferences;
    private Context context;

    public LugarInteractorImpl(EventBus eventBus, ServiceRequest request, PreferencesManager preferences, Context context) {
        this.eventBus=eventBus;
        this.request= request;
        this.preferences= preferences;
        this.context= context;
    }


    ///Eventos
    private void messageEvent(String message,int type){
        LugarEvent event= new LugarEvent();
        event.setEventType(type);
        event.setMessage(message);
        eventBus.postSticky(event);
    }


}
