package com.example.marlon.miruta.ModulMain.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.marlon.miruta.DataModel.User;
import com.example.marlon.miruta.MobileApplication;
import com.example.marlon.miruta.ModulLogin.ui.LoginActivity;
import com.example.marlon.miruta.ModulMain.MainPressenter;
import com.example.marlon.miruta.ModulMain.di.MainComponent;
import com.example.marlon.miruta.ModulPerfil.di.PerfilComponent;
import com.example.marlon.miruta.ModulPerfil.ui.ListenerPerfilUsuario;
import com.example.marlon.miruta.ModulPerfil.ui.PerfilFragment;
import com.example.marlon.miruta.ModulRuta.ui.RutasFragment;
import com.example.marlon.miruta.R;
import com.example.marlon.miruta.Services.CircleTransformService;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements MainView,
        NavigationView.OnNavigationItemSelectedListener,ListenerPerfilUsuario{

    private DrawerLayout drawerLayout;
    private TextView titeltoolbar,textViewNavegationNombre,getTextViewNavegationEmail;
    private ImageView imageViewHeader;
    private int selectfragment=0;

    @Inject
    MainPressenter pressenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addView();
        addToolBar();
        setFragment(0);
        setupInject();
        pressenter.onCreate();
        pressenter.obtenerPerfilUsuario();
    }

   private void setupInject() {
        MobileApplication application = (MobileApplication) getApplication();
        MainComponent component = application.getMainComponent(this);
        component.inject(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pressenter.onDestroy();
    }

    private void addView(){
        drawerLayout= findViewById(R.id.drawer_layout);
        titeltoolbar= findViewById(R.id.textview_titulotoolbar);

        NavigationView navigationView =findViewById(R.id.nav_view);
        View viewDrawer= navigationView.getHeaderView(0);
        textViewNavegationNombre= viewDrawer.findViewById(R.id.textview_nombrenavegation);
        getTextViewNavegationEmail= viewDrawer.findViewById(R.id.textview_emailnavegation);
        imageViewHeader=viewDrawer.findViewById(R.id.imageview_usernavegation);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void addToolBar(){
        Toolbar toolbar=findViewById(R.id.toolbar_perfil);
        toolbar.setTitle("");
        titeltoolbar.setText(R.string.rutas);
        toolbar.setNavigationIcon(R.drawable.ic_menulista);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else if (selectfragment==1){
           selecRutas();
        }else  {
            showLogout();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_rutas){
            if (selectfragment!=0){
                selecRutas();
            }
        }else  if (id == R.id.nav_userperfil) {
            if (selectfragment!=1){
               selecPerfil();
            }
        } else if (id == R.id.nav_salir) {
            drawerLayout.closeDrawer(GravityCompat.START);
            showLogout();
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setFragment(int position) {
        FragmentManager fragmentManager;
        FragmentTransaction fragmentTransaction;
        switch (position) {
            case 0:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                RutasFragment rutasFragment = new RutasFragment();
                fragmentTransaction.replace(R.id.framelayout_main, rutasFragment);
                fragmentTransaction.commit();
                break;
            case 1:
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                PerfilFragment perfilFragment = new PerfilFragment(this);
                fragmentTransaction.replace(R.id.framelayout_main, perfilFragment);
                fragmentTransaction.commit();
                break;
        }
    }

    private void selecRutas(){
        selectfragment=0;
        titeltoolbar.setText(R.string.rutas);
        drawerLayout.closeDrawer(GravityCompat.START);
        setFragment(0);
    }

    private void selecPerfil(){
        selectfragment=1;
        titeltoolbar.setText(R.string.perfil);
        drawerLayout.closeDrawer(GravityCompat.START);
        setFragment(1);
    }

    @Override
    public void infoHeaderNavegation(User _user){

        if (_user.getUNOMBRE()!=null){
            textViewNavegationNombre.setText(_user.getUNOMBRE().toString());
        }
        if (_user.getUEMAIL()!=null){
            getTextViewNavegationEmail.setText(_user.getUEMAIL().toString());
        }

        Picasso.get()
                .load(getResources().getString(R.string.url_base_foto)+_user.getUFOTO())
                .error(R.drawable.ic_user)
                .transform(new CircleTransformService())
                .into(imageViewHeader);


    }

    private void showLogout(){
       Snackbar snackbar=Snackbar.make(drawerLayout,R.string.salir_app, Snackbar.LENGTH_LONG);
        TextView mainTextView =(snackbar.getView()).findViewById(android.support.design.R.id.snackbar_text);
                snackbar.setAction(R.string.si, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pressenter.logOut();
                        Intent intentLogin= new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(intentLogin);
                        finish();
                    }
                }).setActionTextColor(getResources().getColor(R.color.colorPrimary));
        mainTextView.setTextColor(getResources().getColor(R.color.colorblanco));
        snackbar.show();
    }

    @Override
    public void changesPerfil() {
        pressenter.obtenerPerfilUsuario();
    }
}
