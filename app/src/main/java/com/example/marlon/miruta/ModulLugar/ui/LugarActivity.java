package com.example.marlon.miruta.ModulLugar.ui;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import com.example.marlon.miruta.DataModel.DetalleLugar;
import com.example.marlon.miruta.DataModel.LugarTuristico;
import com.example.marlon.miruta.DataModel.Ruta;
import com.example.marlon.miruta.MobileApplication;
import com.example.marlon.miruta.ModulLugar.LugarPressenter;
import com.example.marlon.miruta.ModulLugar.di.LugarComponent;
import com.example.marlon.miruta.ModulServicio.iu.ServicioActivity;
import com.example.marlon.miruta.R;
import com.google.gson.Gson;

import javax.inject.Inject;

public class LugarActivity  extends AppCompatActivity implements LugarView, AdaptadorServicioListener {

    private Ruta ruta;
    private LugarTuristico lugarTuristico;
    private TextView textViewtituloLugar,textViewDescripLugar,titeltoolbar;
    private RecyclerView recyclerViewServicios;
    private ConstraintLayout viewLugar;

    @Inject
    LugarPressenter pressenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lugarturistico);
        getBundle();
        addView();
        addToolBar();
        setupInject();
        pressenter.onCreate();
        showRecyclerServicios();
    }

    private void setupInject() {
        MobileApplication application = (MobileApplication) getApplication();
        LugarComponent component = application.getLugarcomponent(this);
        component.inject(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pressenter.onDestroy();
    }

    //Obtiene el bundle del activityMapa
    public void getBundle() {
        String rutaBundle= getIntent().getExtras().getString("RutaBundle");
        String lugarBlundle= getIntent().getExtras().getString("LugarBundle");
        Gson gson= new Gson();
        ruta= gson.fromJson(rutaBundle,Ruta.class);
        lugarTuristico= gson.fromJson(lugarBlundle,LugarTuristico.class);
    }

    private void addView() {
        titeltoolbar= findViewById(R.id.textview_titulotoolbar);

        textViewtituloLugar= findViewById(R.id.text_tituloLugar);
        textViewtituloLugar.setText(lugarTuristico.getLTNOMBRE());

        textViewDescripLugar=findViewById(R.id.textview_descripcionLugar);
        textViewDescripLugar.setText(lugarTuristico.getLTDESCRIPCION());

        recyclerViewServicios= findViewById(R.id.recyclerViewServicios);
        recyclerViewServicios.setVisibility(View.GONE);

        viewLugar= findViewById(R.id.constraintLayoutLugar);
    }

    private void addToolBar() {
        Toolbar toolbar=findViewById(R.id.toolbar_lugar);
        toolbar.setTitle("");
        titeltoolbar.setText(R.string.lugar_turistico);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    //Muestra la lista de los servicios
    private void showRecyclerServicios(){
        recyclerViewServicios.setVisibility(View.VISIBLE);
        recyclerViewServicios.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL,false));
        AdaptadorServicio adapter= new AdaptadorServicio(lugarTuristico.getDETALLESLTS(),this);
        recyclerViewServicios.setAdapter(adapter);

        LayoutAnimationController controller= AnimationUtils.loadLayoutAnimation
                (recyclerViewServicios.getContext(),R.anim.animlt_lefttoright);
        recyclerViewServicios.setLayoutAnimation(controller);
        recyclerViewServicios.getAdapter().notifyDataSetChanged();
        recyclerViewServicios.scheduleLayoutAnimation();
    }

    //Muestra el snackBar
    @Override
    public void showSnakbar(String _mensaje) {
        Snackbar mSnackBar = Snackbar.make(viewLugar, _mensaje, Snackbar.LENGTH_LONG);
        TextView mainTextView =(mSnackBar.getView()).findViewById(android.support.design.R.id.snackbar_text);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            mainTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }else{
            mainTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        mainTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        mainTextView.setTextColor(getResources().getColor(R.color.colorblanco));
        mSnackBar.show();
    }

    //Accion al seleccionar un servicio de la lista
    @Override
    public void selectServicio(DetalleLugar _detallelugar) {
        Intent servicioIntent= new Intent(this, ServicioActivity.class);
        servicioIntent.putExtra("RutaBundle",ruta.makeJson());
        servicioIntent.putExtra("LugarBundle",lugarTuristico.makeJson());
        servicioIntent.putExtra("ServicioBundle",_detallelugar.makeJson());
        startActivity(servicioIntent);
    }
}
