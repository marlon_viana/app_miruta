package com.example.marlon.miruta.Services;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PreferencesModule {

    public PreferencesModule() {
    }

    @Provides
    @Singleton
    PreferencesManager providesPreferencesManager(SharedPreferences sharedPreferences){
        return new PreferencesManager(sharedPreferences);
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}
