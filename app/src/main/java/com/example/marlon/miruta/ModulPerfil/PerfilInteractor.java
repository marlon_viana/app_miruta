package com.example.marlon.miruta.ModulPerfil;

import java.io.File;

public interface PerfilInteractor {
    void obtenerPerfilUsuario();
    void actualizarUsuario(String _alia,String _nombre,String _telefono);
    void enviarImagen(File _file);
}
