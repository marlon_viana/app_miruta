package com.example.marlon.miruta.ModulMain;

import android.content.Context;

import com.example.marlon.miruta.DataModel.User;
import com.example.marlon.miruta.ModulMain.event.MainEvent;
import com.example.marlon.miruta.Services.PreferencesManager;
import com.example.marlon.miruta.Services.ServiceRequest;
import com.example.marlon.miruta.lib.base.EventBus;

import java.util.ArrayList;
import java.util.List;


public class MainInteractorImpl  implements MainInteractor{

    private EventBus eventBus;
    private ServiceRequest request;
    private PreferencesManager preferences;
    private Context context;

    public MainInteractorImpl(EventBus eventBus, ServiceRequest request, PreferencesManager preferences, Context context) {
        this.eventBus = eventBus;
        this.request = request;
        this.preferences = preferences;
        this.context= context;
    }

    //obtiene la informacion para mostrar en el header del navegationDrawer
    @Override
    public void infoHeader() {
        User user= preferences.getUser();
        userEvent(user,MainEvent.infoHeader);
    }

    //Realica el logout del usuario
    @Override
    public void logOut() {
        preferences.logout(false);
    }

    //Eventos
    private void userEvent(User user, int type){
        MainEvent event= new MainEvent();
        event.setEventType(type);
        event.setUser(user);
        eventBus.postSticky(event);
    }
}
