package com.example.marlon.miruta.DataModel;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("ID_USUARIOS")
    @Expose
    private Integer iDUSUARIOS;
    @SerializedName("U_ALIAS")
    @Expose
    private Object uALIAS;
    @SerializedName("U_EMAIL")
    @Expose
    private String uEMAIL;
    @SerializedName("U_NOMBRE")
    @Expose
    private String uNOMBRE;
    @SerializedName("U_APELLIDO")
    @Expose
    private String uAPELLIDO;
    @SerializedName("U_DNI")
    @Expose
    private String uDNI;
    @SerializedName("U_FECHA_NACIMIENTO")
    @Expose
    private String uFECHANACIMIENTO;
    @SerializedName("U_SEXO")
    @Expose
    private String uSEXO;
    @SerializedName("U_DIRECCION")
    @Expose
    private String uDIRECCION;
    @SerializedName("U_TELEFONO")
    @Expose
    private String uTELEFONO;
    @SerializedName("U_FOTO")
    @Expose
    private String uFOTO;

    public Integer getIDUSUARIOS() {
        return iDUSUARIOS;
    }

    public void setIDUSUARIOS(Integer iDUSUARIOS) {
        this.iDUSUARIOS = iDUSUARIOS;
    }

    public Object getUALIAS() {
        return uALIAS;
    }

    public void setUALIAS(Object uALIAS) {
        this.uALIAS = uALIAS;
    }

    public String getUEMAIL() {
        return uEMAIL;
    }

    public void setUEMAIL(String uEMAIL) {
        this.uEMAIL = uEMAIL;
    }

    public String getUNOMBRE() {
        return uNOMBRE;
    }

    public void setUNOMBRE(String uNOMBRE) {
        this.uNOMBRE = uNOMBRE;
    }

    public String getUAPELLIDO() {
        return uAPELLIDO;
    }

    public void setUAPELLIDO(String uAPELLIDO) {
        this.uAPELLIDO = uAPELLIDO;
    }

    public String getUDNI() {
        return uDNI;
    }

    public void setUDNI(String uDNI) {
        this.uDNI = uDNI;
    }

    public String getUFECHANACIMIENTO() {
        return uFECHANACIMIENTO;
    }

    public void setUFECHANACIMIENTO(String uFECHANACIMIENTO) {
        this.uFECHANACIMIENTO = uFECHANACIMIENTO;
    }

    public String getUSEXO() {
        return uSEXO;
    }

    public void setUSEXO(String uSEXO) {
        this.uSEXO = uSEXO;
    }

    public String getUDIRECCION() {
        return uDIRECCION;
    }

    public void setUDIRECCION(String uDIRECCION) {
        this.uDIRECCION = uDIRECCION;
    }

    public String getUTELEFONO() {
        return uTELEFONO;
    }

    public void setUTELEFONO(String uTELEFONO) {
        this.uTELEFONO = uTELEFONO;
    }

    public String getUFOTO() {
        return uFOTO;
    }

    public void setUFOTO(String uFOTO) {
        this.uFOTO = uFOTO;
    }

    public String makeJson(){
        Gson gson= new Gson();
        String _jsonstring= gson.toJson(this);
        return  _jsonstring;
    }
}
