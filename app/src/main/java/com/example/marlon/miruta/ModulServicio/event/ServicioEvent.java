package com.example.marlon.miruta.ModulServicio.event;

import java.util.List;

public class ServicioEvent {
    public final static int showSnack= 0;
    public final static int showComentarios=1;
    public final static int showRatingGeneral=2;
    public final static int getIdUser=4;

    private String Message;
    private int eventType;
    private List list;
    private int valorint;

    public int getValorint() {
        return valorint;
    }

    public void setValorint(int valorint) {
        this.valorint = valorint;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
