package com.example.marlon.miruta.ModulMaps;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.marlon.miruta.DataModel.LugarTuristico;
import com.example.marlon.miruta.DataModel.Nodo;
import com.example.marlon.miruta.ModulMaps.event.MapsEvent;
import com.example.marlon.miruta.R;
import com.example.marlon.miruta.Services.PreferencesManager;
import com.example.marlon.miruta.Services.ServiceConectivity;
import com.example.marlon.miruta.Services.ServiceRequest;
import com.example.marlon.miruta.lib.base.EventBus;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsInteractorImpl implements MapsInteractor {
    private EventBus eventBus;
    private ServiceRequest request;
    private PreferencesManager preferences;
    private Context context;
    private List<Nodo> listNodo;
    private List<LugarTuristico> listLugarturisitico;

    public MapsInteractorImpl(EventBus eventBus, ServiceRequest request, PreferencesManager preferences, Context context) {
        this.eventBus=eventBus;
        this.request= request;
        this.preferences= preferences;
        this.context= context;
    }

    //Obtiene la listas de nodos
    @Override
    public void getNodos(String _id) {
        if (ServiceConectivity.isNetDisponible(context)){
            messageEvent(context.getResources().getString(R.string.cargando),MapsEvent.showMessageInfo);
            request.nodos(_id).enqueue(new Callback<List<Nodo>>() {
                @Override
                public void onResponse(Call<List<Nodo>> call, Response<List<Nodo>> response) {
                    try {

                        listNodo= response.body();
                        if (listNodo.size()!=0){
                            boolenaEvent(false,MapsEvent.showLayoutMaps);
                            listEvent(listNodo,MapsEvent.showNodos);
                            clicloNodos();
                        }else {
                            boolenaEvent(true,MapsEvent.showLayoutMaps);
                            messageEvent(context.getResources().getString(R.string.no_nod)
                                    ,MapsEvent.showMessageInfo);
                        }



                    }catch (Exception e){
                        Log.e("ERROR NODO RESPUESTA",e.toString());
                    }
                }

                @Override
                public void onFailure(Call<List<Nodo>> call, Throwable t) {
                    boolenaEvent(true,MapsEvent.showLayoutMaps);
                    messageEvent(context.getResources().getString(R.string.no_api)
                            ,MapsEvent.showMessageInfo);
                }
            });

        }else {
            boolenaEvent(true,MapsEvent.showLayoutMaps);
            messageEvent(context.getResources().getString(R.string.no_internet)
                    ,MapsEvent.showMessageInfo);
        }

    }

    ///Obtiene el nodo origen y el nodo destino de la lista de nodos
    private void clicloNodos(){
        int tamañolista=listNodo.size();
        if (tamañolista>=2){
            for (int i=1; i<tamañolista;i++){
               crearRuta( listNodo.get(i-1),listNodo.get(i));
            }
        }
    }

    ///Genera la url para consumir el api de google para unir los nodos
    private void crearRuta(Nodo nodoOrigen,Nodo nodoDestino){

        Double latOrigen= Double.valueOf(nodoOrigen.getNOLATITUD());
        Double longOrigin=Double.valueOf(nodoOrigen.getNOLONGITUD());
        Double latDestino=Double.valueOf(nodoDestino.getNOLATITUD());
        Double longDestino=Double.valueOf(nodoDestino.getNOLONGITUD());

        String url = makeURL(latOrigen, longOrigin, latDestino, longDestino);

        //Creating a string request
        StringRequest stringRequest = new StringRequest(url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        drawPath(response);
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //Adding the request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    ///Crea la url para consumir el api de google que hace las Polyline
    private String makeURL (double sourcelat, double sourcelog, double destlat, double destlog ){
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString
                .append(Double.toString( sourcelog));
        urlString.append("&destination=");// to
        urlString
                .append(Double.toString( destlat));
        urlString.append(",");
        urlString.append(Double.toString(destlog));
        urlString.append("&sensor=false&mode=driving&alternatives=true");
        urlString.append("&AIzaSyDKD5YxFLmlN7lrNBrW5wSbAWh9VTb03k8");
        return urlString.toString();
    }

    //Genera el formato de las PolyLine
    public void drawPath(String  result) {
        try {
            final JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            decodePoly(encodedString);
        }
        catch (JSONException e) {
            Log.e("ERROR","PARSION JSON RUTA");
        }
    }

    //Genera las polyLine que se mostraran en el mapa
    private void decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng( (((double) lat / 1E5)),
                    (((double) lng / 1E5) ));
            poly.add(p);
        }

        listEvent(poly,MapsEvent.showPoly);
    }

    ///Eventos
    private void messageEvent(String message,int type){
        MapsEvent event= new MapsEvent();
        event.setEventType(type);
        event.setMessage(message);
        eventBus.postSticky(event);
    }

    private void boolenaEvent(Boolean _boolean,int type){
        MapsEvent event= new MapsEvent();
        event.setEventType(type);
        event.setaBoolean(_boolean);
        eventBus.postSticky(event);
    }

    private void listEvent(List list, int type){
        MapsEvent event= new MapsEvent();
        event.setEventType(type);
        event.setList(list);
        eventBus.postSticky(event);
    }
}
