package com.example.marlon.miruta.ModulMain;
import com.example.marlon.miruta.ModulMain.event.MainEvent;
import com.example.marlon.miruta.ModulMain.ui.MainView;
import com.example.marlon.miruta.lib.base.EventBus;

import org.greenrobot.eventbus.Subscribe;

public class MainPressenterImpl implements MainPressenter {

    private EventBus eventBus;
    private MainView view;
    private MainInteractor interactor;

    public MainPressenterImpl(EventBus eventBus, MainView view, MainInteractor interactor) {
        this.eventBus = eventBus;
        this.view = view;
        this.interactor = interactor;
    }


    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Subscribe
    @Override
    public void onEventMainThread(MainEvent event) {

        switch (event.getEventType()){
            case MainEvent.infoHeader:
                    view.infoHeaderNavegation(event.getUser());
                break;
        }

    }

    @Override
    public void logOut() {
        interactor.logOut();
    }

    @Override
    public void obtenerPerfilUsuario() {
        interactor.infoHeader();
    }
}
