package com.example.marlon.miruta.ModulRuta.ui;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import com.example.marlon.miruta.DataModel.Ruta;
import com.example.marlon.miruta.MobileApplication;
import com.example.marlon.miruta.ModulLogin.ui.LoginActivity;
import com.example.marlon.miruta.ModulMaps.iu.MapaActivity;
import com.example.marlon.miruta.ModulRuta.RutasPressenter;
import com.example.marlon.miruta.ModulRuta.di.RutasComponent;
import com.example.marlon.miruta.R;

import java.util.List;

import javax.inject.Inject;

public class RutasFragment extends Fragment implements RutasView,AdaptadorRutasListener{

    RecyclerView recyclerViewRutas;
    ConstraintLayout constraintLayoutrutainfo;
    TextView textViewInfo;
    View v;

    @Inject
    RutasPressenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v= inflater.inflate(R.layout.fragmentlayout_useriorutas, container, false);
        addView();
        setupInject();
        presenter.onCreate();
        return v;
    }

    private void setupInject() {
        MobileApplication application= (MobileApplication) getActivity().getApplication();
        RutasComponent component= application.getRutasComponent(this);
        component.inject(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    private void addView(){
        recyclerViewRutas= v.findViewById(R.id.recyclerViewRutas);

        constraintLayoutrutainfo= v.findViewById(R.id.contenidoUbicacion);
        textViewInfo= v.findViewById(R.id.textview_infoRutas);
    }

    @Override
    public void showLayoutRuta(boolean _boolean) {
        if (_boolean){
            constraintLayoutrutainfo.setVisibility(View.VISIBLE);
        }else {
            constraintLayoutrutainfo.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void showMessageLayout(String _message) {
        textViewInfo.setText(_message);
    }

    @Override
    public void showMessageOption(String _message, final String selection) {
        Snackbar snackbar=Snackbar.make(v,_message, Snackbar.LENGTH_LONG);
        TextView mainTextView =(snackbar.getView()).findViewById(android.support.design.R.id.snackbar_text);
        snackbar.setAction(R.string.si, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.borrarRuta(selection);
            }
        }).setActionTextColor(getResources().getColor(R.color.colorPrimary));
        mainTextView.setTextColor(getResources().getColor(R.color.colorblanco));
        snackbar.show();
    }

    @Override
    public void showMessage(String _message) {
        Snackbar mSnackBar = Snackbar.make(v, _message, Snackbar.LENGTH_LONG);
        TextView mainTextView =(mSnackBar.getView()).findViewById(android.support.design.R.id.snackbar_text);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            mainTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }else{
            mainTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        mainTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        mainTextView.setTextColor(getResources().getColor(R.color.colorblanco));
        mSnackBar.show();
    }

    @Override
    public void showRecyclerView(List _list) {
        recyclerViewRutas.setVisibility(View.VISIBLE);
        recyclerViewRutas.setLayoutManager(new LinearLayoutManager(getActivity(),
                                            LinearLayoutManager.VERTICAL,false));
        AdaptadorRutas adapter= new AdaptadorRutas(_list,this);
        recyclerViewRutas.setAdapter(adapter);

        LayoutAnimationController controller=AnimationUtils.loadLayoutAnimation
                                            (recyclerViewRutas.getContext(),R.anim.animlt_lefttoright);
        recyclerViewRutas.setLayoutAnimation(controller);
        recyclerViewRutas.getAdapter().notifyDataSetChanged();
        recyclerViewRutas.scheduleLayoutAnimation();
    }


    @Override
    public void selctRuta(Ruta _ruta) {
        Intent map= new Intent(getActivity(),MapaActivity.class);
        map.putExtra("RutaBundle",_ruta.makeJson());
        startActivity(map);
    }

    @Override
    public void deletRuta(Ruta _ruta) {
        String message= getString(R.string.elimer_ruta)+" "+_ruta.getRTNOMBRE();
        showMessageOption(message,_ruta.getIDRUTA());
    }

    @Override
    public void shareRuta(Ruta _ruta) {
        String titulo= getString(R.string.compartir_ruta)+" "+_ruta.getRTNOMBRE();
        String body="https://preuniversitarioaprender.com/index.php/welcome/consultaRutaPublicidad/"
                    +_ruta.getIDRUTA();

        Intent intentShare=  new Intent(Intent.ACTION_SEND);
        intentShare.setType("text/plain");
        String stringBody=body;
        String stringSub="Ruta 1";
        intentShare.putExtra(Intent.EXTRA_SUBJECT,stringSub);
        intentShare.putExtra(Intent.EXTRA_TEXT,stringBody);
        getActivity().startActivity(Intent.createChooser(intentShare,titulo));
    }
}
