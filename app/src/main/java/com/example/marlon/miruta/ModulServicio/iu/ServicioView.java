package com.example.marlon.miruta.ModulServicio.iu;

import java.util.List;

public interface ServicioView {

    void showSnack(String _message);
    void showComentarios(List _list);
    void showRaiting(int _rating);
    void getIdUser(String _iduser);
}
