package com.example.marlon.miruta.ModulLugar;

import com.example.marlon.miruta.ModulLugar.event.LugarEvent;
import com.example.marlon.miruta.ModulLugar.ui.LugarView;
import com.example.marlon.miruta.lib.base.EventBus;

import org.greenrobot.eventbus.Subscribe;

public class LugarPressenterImpl implements LugarPressenter {
    private EventBus eventBus;
    private LugarView view;
    private LugarInteractor interactor;

    public LugarPressenterImpl(EventBus eventBus, LugarView view, LugarInteractor interactor) {
        this.eventBus=eventBus;
        this.view= view;
        this.interactor= interactor;
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Subscribe
    @Override
    public void onEventMainThread(LugarEvent event) {
        switch (event.getEventType()){
            case LugarEvent.showSnack:
                view.showSnakbar(event.getMessage());
                break;
        }
    }

}
