package com.example.marlon.miruta.ModulPerfil.event;

import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;

import com.example.marlon.miruta.DataModel.User;

import java.util.List;

public class PerfilEvent {

    public final static int showPerfilUsuario = 0;
    public final static int statusButton=1;
    public final static int showSnakBar=2;
    public final static int showCambios=3;

    private int eventType;
    private String Message;
    private List list;
    private User user;
    private Boolean aBoolean;


    public Boolean getaBoolean() {
        return aBoolean;
    }

    public void setaBoolean(Boolean aBoolean) {
        this.aBoolean = aBoolean;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String sendMessage) {
        this.Message = sendMessage;
    }

}
