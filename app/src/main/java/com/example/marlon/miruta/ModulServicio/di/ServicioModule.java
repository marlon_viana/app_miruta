package com.example.marlon.miruta.ModulServicio.di;

import android.content.Context;

import com.example.marlon.miruta.ModulServicio.ServicioInteractor;
import com.example.marlon.miruta.ModulServicio.ServicioInteractorImpl;
import com.example.marlon.miruta.ModulServicio.ServicioPressenter;
import com.example.marlon.miruta.ModulServicio.ServicioPressenterImpl;
import com.example.marlon.miruta.ModulServicio.iu.ServicioView;
import com.example.marlon.miruta.Services.PreferencesManager;
import com.example.marlon.miruta.Services.ServiceRequest;
import com.example.marlon.miruta.lib.base.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ServicioModule {

    private ServicioView view;

    public ServicioModule(ServicioView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    ServicioPressenter providerServicioPressenter(EventBus eventBus, ServicioView view, ServicioInteractor interactor){
        return new ServicioPressenterImpl(eventBus, view, interactor) {
        };
    }
    @Provides
    @Singleton
    ServicioInteractor providerServicioInteractor(EventBus eventBus, ServiceRequest request, PreferencesManager preferences, Context context){
        return new ServicioInteractorImpl(eventBus, request, preferences, context) {
        };
    }

    @Provides
    @Singleton
    ServicioView providerServicioView(){
        return this.view;
    }
}
