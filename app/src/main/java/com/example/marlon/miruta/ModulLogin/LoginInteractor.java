package com.example.marlon.miruta.ModulLogin;

public interface LoginInteractor {
    void sendLogin(String _email,String _pass);
    void validarLogin();
}
