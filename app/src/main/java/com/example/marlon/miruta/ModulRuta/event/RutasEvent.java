package com.example.marlon.miruta.ModulRuta.event;

import java.util.List;

public class RutasEvent {

    public final static int showMessageRuta= 0;
    public final static int showLayoutRuta=1;
    public final static int showRecyclerView=2;
    public final static int showMessage=3;


    private String Message;
    private int eventType;
    private Boolean aBoolean;
    private List list;

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public Boolean getaBoolean() {
        return aBoolean;
    }

    public void setaBoolean(Boolean aBoolean) {
        this.aBoolean = aBoolean;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }
}
