package com.example.marlon.miruta.ModulMaps;
import com.example.marlon.miruta.ModulMaps.event.MapsEvent;

public interface MapsPressenter {

    void onCreate();
    void onDestroy();
    void getNodos(String _id);
    void onEventMainThread(MapsEvent event);

}
