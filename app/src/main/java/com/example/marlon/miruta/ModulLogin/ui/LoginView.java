package com.example.marlon.miruta.ModulLogin.ui;

public interface LoginView {
    void starActivityLogin();
    void showMessageLogin(String _mensaje);
    void statusButton(Boolean _boolen);
    void showSnackBar(String _mensaje);
    void validarLogin(boolean _boolean);
}
