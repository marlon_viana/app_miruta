package com.example.marlon.miruta.ModulMain.di;


import android.content.Context;

import com.example.marlon.miruta.ModulMain.MainInteractor;
import com.example.marlon.miruta.ModulMain.MainInteractorImpl;
import com.example.marlon.miruta.ModulMain.MainPressenter;
import com.example.marlon.miruta.ModulMain.MainPressenterImpl;
import com.example.marlon.miruta.ModulMain.ui.MainView;
import com.example.marlon.miruta.Services.PreferencesManager;
import com.example.marlon.miruta.Services.ServiceRequest;
import com.example.marlon.miruta.lib.base.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {

    private MainView view;

    public MainModule(MainView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    MainPressenter providerMainPressenter(EventBus eventBus, MainView view, MainInteractor interactor){
        return new MainPressenterImpl(eventBus, view, interactor);
    }
    @Provides
    @Singleton
    MainInteractor providerMainInteractor(EventBus eventBus, ServiceRequest request, PreferencesManager preferences, Context context){
        return new MainInteractorImpl(eventBus, request, preferences, context);
    }

    @Provides
    @Singleton
    MainView providerMainView(){
        return this.view;
    }
}
