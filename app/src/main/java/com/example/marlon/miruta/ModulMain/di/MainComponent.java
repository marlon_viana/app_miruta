package com.example.marlon.miruta.ModulMain.di;

import com.example.marlon.miruta.ModulMain.ui.MainActivity;
import com.example.marlon.miruta.Services.ContextModule;
import com.example.marlon.miruta.Services.PreferencesModule;
import com.example.marlon.miruta.Services.ServiceRequestModule;
import com.example.marlon.miruta.lib.di.LibsModule;
import javax.inject.Singleton;
import dagger.Component;

@Singleton
@Component(modules = {LibsModule.class, MainModule.class, ContextModule.class,ServiceRequestModule.class, PreferencesModule.class})
public interface MainComponent {
    void inject(MainActivity activity);
}