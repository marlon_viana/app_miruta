package com.example.marlon.miruta.ModulPerfil.di;

import android.content.Context;

import com.example.marlon.miruta.ModulPerfil.PerfilInteractor;
import com.example.marlon.miruta.ModulPerfil.PerfilInteractorImpl;
import com.example.marlon.miruta.ModulPerfil.PerfilPressenter;
import com.example.marlon.miruta.ModulPerfil.PerfilPressenterImpl;
import com.example.marlon.miruta.ModulPerfil.ui.PerfilView;
import com.example.marlon.miruta.Services.PreferencesManager;
import com.example.marlon.miruta.Services.ServiceRequest;
import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PerfilModule {
    private PerfilView view;

    public PerfilModule(PerfilView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    PerfilPressenter providerPerfilPressenter(EventBus eventBus, PerfilView view, PerfilInteractor interactor){
        return new PerfilPressenterImpl(eventBus, view, interactor);
    }
    @Provides
    @Singleton
    PerfilInteractor providerPerfilInteractor(EventBus eventBus, ServiceRequest request, PreferencesManager preferences, Context context){
        return new PerfilInteractorImpl(eventBus, request, preferences, context);
    }

    @Provides
    @Singleton
    PerfilView providerPerfilView(){
        return this.view;
    }
}
