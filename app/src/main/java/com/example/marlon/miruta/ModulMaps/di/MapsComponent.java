package com.example.marlon.miruta.ModulMaps.di;
import com.example.marlon.miruta.ModulMaps.iu.MapaActivity;
import com.example.marlon.miruta.Services.ContextModule;
import com.example.marlon.miruta.Services.PreferencesModule;
import com.example.marlon.miruta.Services.ServiceRequestModule;
import com.example.marlon.miruta.lib.di.LibsModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {LibsModule.class, MapsModule.class, ContextModule.class,ServiceRequestModule.class, PreferencesModule.class})
public interface MapsComponent {
    void inject(MapaActivity activity);
}
