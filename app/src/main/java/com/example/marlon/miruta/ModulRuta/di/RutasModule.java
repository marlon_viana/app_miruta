package com.example.marlon.miruta.ModulRuta.di;


import android.content.Context;

import com.example.marlon.miruta.ModulRuta.RutasInteractor;
import com.example.marlon.miruta.ModulRuta.RutasInteractorImpl;
import com.example.marlon.miruta.ModulRuta.RutasPressenter;
import com.example.marlon.miruta.ModulRuta.RutasPressenterImpl;
import com.example.marlon.miruta.ModulRuta.ui.RutasView;
import com.example.marlon.miruta.Services.PreferencesManager;
import com.example.marlon.miruta.Services.ServiceRequest;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RutasModule {
    private RutasView view;

    public RutasModule(RutasView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    RutasPressenter providerRutasPressenter(EventBus eventBus, RutasView view, RutasInteractor interactor){
        return new RutasPressenterImpl(eventBus, view, interactor);
    }
    @Provides
    @Singleton
    RutasInteractor providerRutasInteractor(EventBus eventBus, ServiceRequest request, PreferencesManager preferences, Context context){
        return new RutasInteractorImpl(eventBus, request, preferences, context);
    }

    @Provides
    @Singleton
    RutasView providerRutasView(){
        return this.view;
    }
}
