package com.example.marlon.miruta.ModulLugar.ui;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;
import com.example.marlon.miruta.DataModel.DetalleLugar;
import com.example.marlon.miruta.R;

import java.util.List;

class AdaptadorServicio extends RecyclerView.Adapter<AdaptadorServicio.ViewHolderData> {


    private AdaptadorServicioListener listener;
    private List<DetalleLugar> items;

    public AdaptadorServicio (List<DetalleLugar> items, AdaptadorServicioListener listener) {
        this.items = items;
        this.listener= listener;
    }

    public class ViewHolderData extends RecyclerView.ViewHolder {

        TextView textViewTitle;
        Context context;

        public ViewHolderData(View itemView) {
            super(itemView);
            textViewTitle= itemView.findViewById(R.id.textview_tituloServicioCard);
            context= itemView.getContext();
        }
    }

    @NonNull
    @Override
    public AdaptadorServicio.ViewHolderData onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_cardservicio,viewGroup,false);
        return new AdaptadorServicio.ViewHolderData(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdaptadorServicio.ViewHolderData viewHolderData, final int i) {
        viewHolderData.textViewTitle.setText(items.get(i).getDLTSNOMBRE());
        viewHolderData.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.selectServicio(items.get(i));
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
