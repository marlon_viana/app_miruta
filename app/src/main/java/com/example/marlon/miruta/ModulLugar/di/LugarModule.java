package com.example.marlon.miruta.ModulLugar.di;

import android.content.Context;

import com.example.marlon.miruta.ModulLugar.LugarInteractor;
import com.example.marlon.miruta.ModulLugar.LugarInteractorImpl;
import com.example.marlon.miruta.ModulLugar.LugarPressenter;
import com.example.marlon.miruta.ModulLugar.LugarPressenterImpl;
import com.example.marlon.miruta.ModulLugar.ui.LugarView;
import com.example.marlon.miruta.Services.PreferencesManager;
import com.example.marlon.miruta.Services.ServiceRequest;
import com.example.marlon.miruta.lib.base.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LugarModule {

    private LugarView view;

    public LugarModule(LugarView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    LugarPressenter providerLugarPressenter(EventBus eventBus, LugarView view, LugarInteractor interactor){
        return new LugarPressenterImpl(eventBus, view, interactor);
    }
    @Provides
    @Singleton
    LugarInteractor providerLugarInteractor(EventBus eventBus, ServiceRequest request, PreferencesManager preferences, Context context){
        return new LugarInteractorImpl(eventBus, request, preferences, context);
    }

    @Provides
    @Singleton
    LugarView providerLugarView(){
        return this.view;
    }
}
