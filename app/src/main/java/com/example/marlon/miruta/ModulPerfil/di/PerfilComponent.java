package com.example.marlon.miruta.ModulPerfil.di;

import com.example.marlon.miruta.ModulPerfil.ui.PerfilFragment;
import com.example.marlon.miruta.Services.ContextModule;
import com.example.marlon.miruta.Services.PreferencesModule;
import com.example.marlon.miruta.Services.ServiceRequestModule;
import com.example.marlon.miruta.lib.di.LibsModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {LibsModule.class, PerfilModule.class, ContextModule.class, ServiceRequestModule.class, PreferencesModule.class})
public interface PerfilComponent {
    void inject(PerfilFragment activity);
}

