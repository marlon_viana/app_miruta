package com.example.marlon.miruta.ModulPerfil;
import com.example.marlon.miruta.ModulPerfil.event.PerfilEvent;
import java.io.File;

public interface PerfilPressenter {

    void onCreate();
    void onDestroy();
    void onEventMainThread(PerfilEvent event);
    void actualizarUsuario(String _alia,String _nombre,String _telefono);
    void enviarImagen(File _file);
}
