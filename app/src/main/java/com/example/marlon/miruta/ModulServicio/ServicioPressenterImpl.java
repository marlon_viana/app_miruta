package com.example.marlon.miruta.ModulServicio;

import com.example.marlon.miruta.ModulServicio.event.ServicioEvent;
import com.example.marlon.miruta.ModulServicio.iu.ServicioView;
import com.example.marlon.miruta.lib.base.EventBus;

import org.greenrobot.eventbus.Subscribe;

public class ServicioPressenterImpl implements ServicioPressenter{
    private EventBus eventBus;
    private ServicioView view;
    private ServicioInteractor interactor;


    public ServicioPressenterImpl(EventBus eventBus, ServicioView view, ServicioInteractor interactor) {
        this.eventBus= eventBus;
        this.view= view;
        this.interactor= interactor;
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Subscribe
    @Override
    public void onEventMainThread(ServicioEvent event) {

        switch (event.getEventType()){
            case ServicioEvent.showSnack:
                view.showSnack(event.getMessage());
                break;

            case ServicioEvent.showComentarios:
                view.showComentarios(event.getList());
                break;

            case ServicioEvent.showRatingGeneral:
                view.showRaiting(event.getValorint());
                break;

            case ServicioEvent.getIdUser:
                view.getIdUser(event.getMessage());
                break;

        }

    }

    @Override
    public void sendCalificacion(String _iddetalle, String _idcalifacion, String _idruta) {
        interactor.sendCalificacion(_iddetalle,_idcalifacion,_idruta);
    }

    @Override
    public void sendComentario(String _iddetalle, String _titulo, String _comentario) {
        interactor.sendComentario(_iddetalle,_titulo,_comentario);
    }

    @Override
    public void getValoracionComentarios(String _iddetalle, String _idruta) {
        interactor.getValoracionComentarios(_iddetalle,_idruta);
    }

    @Override
    public void borrarComentario(String _idComentario) {
        interactor.deletComentario(_idComentario);
    }
}
