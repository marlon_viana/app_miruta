package com.example.marlon.miruta.ModulLugar.di;


import com.example.marlon.miruta.ModulLugar.ui.LugarActivity;
import com.example.marlon.miruta.Services.ContextModule;
import com.example.marlon.miruta.Services.PreferencesModule;
import com.example.marlon.miruta.Services.ServiceRequestModule;
import com.example.marlon.miruta.lib.di.LibsModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {LibsModule.class, LugarModule.class, ContextModule.class,ServiceRequestModule.class, PreferencesModule.class})
public interface LugarComponent {
    void inject(LugarActivity activity);
}
