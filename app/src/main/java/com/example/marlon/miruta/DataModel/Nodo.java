package com.example.marlon.miruta.DataModel;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Nodo {
    @SerializedName("ID_NODO")
    @Expose
    private String iDNODO;
    @SerializedName("ID_UBICACION")
    @Expose
    private String iDUBICACION;
    @SerializedName("NO_NOMBRE")
    @Expose
    private String nONOMBRE;
    @SerializedName("NO_DIRECCION")
    @Expose
    private String nODIRECCION;
    @SerializedName("NO_LATITUD")
    @Expose
    private String nOLATITUD;
    @SerializedName("NO_LONGITUD")
    @Expose
    private String nOLONGITUD;
    @SerializedName("LUGARES_TURISTICOS")
    @Expose
    private List<LugarTuristico> lUGARESTURISTICOS = null;

    public String getIDNODO() {
        return iDNODO;
    }

    public void setIDNODO(String iDNODO) {
        this.iDNODO = iDNODO;
    }

    public String getIDUBICACION() {
        return iDUBICACION;
    }

    public void setIDUBICACION(String iDUBICACION) {
        this.iDUBICACION = iDUBICACION;
    }

    public String getNONOMBRE() {
        return nONOMBRE;
    }

    public void setNONOMBRE(String nONOMBRE) {
        this.nONOMBRE = nONOMBRE;
    }

    public String getNODIRECCION() {
        return nODIRECCION;
    }

    public void setNODIRECCION(String nODIRECCION) {
        this.nODIRECCION = nODIRECCION;
    }

    public String getNOLATITUD() {
        return nOLATITUD;
    }

    public void setNOLATITUD(String nOLATITUD) {
        this.nOLATITUD = nOLATITUD;
    }

    public String getNOLONGITUD() {
        return nOLONGITUD;
    }

    public void setNOLONGITUD(String nOLONGITUD) {
        this.nOLONGITUD = nOLONGITUD;
    }

    public List<LugarTuristico> getLUGARESTURISTICOS() {
        return lUGARESTURISTICOS;
    }

    public void setLUGARESTURISTICOS(List<LugarTuristico> lUGARESTURISTICOS) {
        this.lUGARESTURISTICOS = lUGARESTURISTICOS;
    }

    public String makeJson(){
        Gson gson= new Gson();
        String _jsonstring= gson.toJson(this);
        return  _jsonstring;
    }
}
