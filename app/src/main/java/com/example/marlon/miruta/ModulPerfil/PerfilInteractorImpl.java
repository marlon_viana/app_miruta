package com.example.marlon.miruta.ModulPerfil;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;

import com.example.marlon.miruta.DataModel.User;
import com.example.marlon.miruta.ModulLogin.event.LoginEvent;
import com.example.marlon.miruta.ModulPerfil.event.PerfilEvent;
import com.example.marlon.miruta.R;
import com.example.marlon.miruta.Services.PreferencesManager;
import com.example.marlon.miruta.Services.ServiceConectivity;
import com.example.marlon.miruta.Services.ServiceRequest;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PerfilInteractorImpl  implements PerfilInteractor {

    private EventBus eventBus;
    private ServiceRequest request;
    private PreferencesManager preferences;
    private Context context;
    private File fileImagen;
    private boolean newFoto=false;


    public PerfilInteractorImpl(EventBus eventBus, ServiceRequest request, PreferencesManager preferences, Context context) {
        this.eventBus = eventBus;
        this.request = request;
        this.preferences = preferences;
        this.context= context;
    }

    ///Obtiene el objeto usuario guardado como preferencia al hacer login
    @Override
    public void obtenerPerfilUsuario() {
        User user= preferences.getUser();
        userEvent(user,PerfilEvent.showPerfilUsuario);
    }

    @Override
    public void actualizarUsuario(String _alia, String _nombre, String _telefono) {
        User user= preferences.getUser();

        if (ServiceConectivity.isNetDisponible(context)){
            boolenaEvent(false,PerfilEvent.statusButton);

            request.actualizarusuario(user.getIDUSUARIOS().toString(),_alia,_nombre
                    ,user.getUAPELLIDO(),user.getUEMAIL()
                    ,user.getUDNI()
                    ,user.getUFECHANACIMIENTO()
                    ,user.getUSEXO()
                    ,user.getUDIRECCION()
                    ,_telefono,user.getUFOTO()).enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {
                            boolenaEvent(true,PerfilEvent.statusButton);
                            try {
                                User userrespuesta= response.body();
                                preferences.setUser(userrespuesta);
                                messageEvent(context.getString(R.string.exito_cambios)
                                        ,PerfilEvent.showSnakBar);
                                eventType(PerfilEvent.showCambios);

                                if (newFoto){
                                    subirImagen();
                                }

                            }catch (Exception e){
                                Log.e("ERROR CAMBIOS", e.toString());
                                messageEvent(context.getString(R.string.error_cambios)
                                        ,PerfilEvent.showSnakBar);
                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            boolenaEvent(true,PerfilEvent.statusButton);
                            messageEvent(context.getString(R.string.error_cambios)
                                    ,PerfilEvent.showSnakBar);
                        }
                    });


        }else {
            boolenaEvent(true,PerfilEvent.statusButton);
            messageEvent(context.getString(R.string.no_internet)
                    ,PerfilEvent.showSnakBar);
        }

    }

    @Override
    public void enviarImagen(File _file) {
        fileImagen=_file;
        newFoto=true;
    }


    public void subirImagen(){
        MultipartBody.Part image = MultipartBody.Part.createFormData("image", fileImagen.getName()
                                        , RequestBody.create(MediaType.parse("image/*"), fileImagen));

        RequestBody iduser= RequestBody.create(MediaType.parse("text/plain"),preferences.getUser().getIDUSUARIOS().toString());

         request.postImage(image,iduser).enqueue(new Callback<User>() {
             @Override
             public void onResponse(Call<User> call, Response<User> response) {
                 try {
                     User user= response.body();
                     preferences.setUser(user);
                     messageEvent(context.getString(R.string.exito_foto),PerfilEvent.showSnakBar);
                     newFoto=false;
                     eventType(PerfilEvent.showCambios);
                 }catch (Exception e){
                     Log.e("ERROR FOTO",e.toString());
                 }

             }

             @Override
             public void onFailure(Call<User> call, Throwable t) {
                 Log.e("ERROR FOTO","FALLO");
                 messageEvent(context.getString(R.string.error_foto),PerfilEvent.showSnakBar);
             }
         });
    }


    //EVENTOS

    private void messageEvent(String message,int type){
        PerfilEvent event= new PerfilEvent();
        event.setEventType(type);
        event.setMessage(message);
        eventBus.postSticky(event);
    }

    private void userEvent(User user, int type){
        PerfilEvent event= new PerfilEvent();
        event.setEventType(type);
        event.setUser(user);
        eventBus.postSticky(event);
    }

    private void boolenaEvent(Boolean _boolean,int type){
        PerfilEvent event= new PerfilEvent();
        event.setEventType(type);
        event.setaBoolean(_boolean);
        eventBus.postSticky(event);
    }

    private void eventType(int type){
        PerfilEvent event= new PerfilEvent();
        event.setEventType(type);
        eventBus.postSticky(event);
    }
}
