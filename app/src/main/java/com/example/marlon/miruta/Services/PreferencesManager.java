package com.example.marlon.miruta.Services;

import android.content.SharedPreferences;

import com.example.marlon.miruta.DataModel.User;
import com.google.gson.Gson;


public class PreferencesManager {

    private static final String KEY_USER = "key_user";
    private static final String KEY_LOGGED = "key_logged";

    private SharedPreferences preferences;

    public PreferencesManager(SharedPreferences preferences) {
        this.preferences = preferences;
    }


    /// USUARIO
    public void setUser(User user){
        Gson gson = new Gson();
        String jsonString = gson.toJson(user);
        SharedPreferences.Editor editor= preferences.edit();
        editor.putString(KEY_USER, jsonString);
        editor.apply();
    }


    public User getUser(){
        String json_user= preferences.getString(KEY_USER, null);
        if(json_user==null)
            return null;
        else {
            Gson gson = new Gson();
            User usuario = gson.fromJson(json_user, User.class);
            return usuario;
        }
    }

    public void clearUser(){

        if (preferences.contains(KEY_USER)) {
            SharedPreferences.Editor editor= preferences.edit();
            editor.remove(KEY_USER).clear();
            editor.apply();
        }
    }

    //LOGIN
    public boolean isLogged(){
        return preferences.getBoolean(KEY_LOGGED, false);
    }

    public void logout(Boolean aboolena){
        SharedPreferences.Editor editor= preferences.edit();
        editor.putBoolean(KEY_LOGGED, aboolena);
        editor.apply();
    }

}
