package com.example.marlon.miruta.ModulLogin;

import com.example.marlon.miruta.ModulLogin.event.LoginEvent;
import com.example.marlon.miruta.ModulLogin.ui.LoginView;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class LoginPressenterImpl implements LoginPressenter {

    private EventBus eventBus;
    private LoginView view;
    private LoginInteractor interactor;

    public LoginPressenterImpl(EventBus eventBus, LoginView view, LoginInteractor interactor) {
        this.eventBus = eventBus;
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
        interactor.validarLogin();
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }


    @Subscribe
    @Override
    public void onEventMainThread(LoginEvent event) {

        switch (event.getEventType()){
            case LoginEvent.nextActivity:
                view.starActivityLogin();
                break;

            case LoginEvent.showMessage:
                view.showMessageLogin(event.getMessage());
                break;

            case LoginEvent.statusButton:
                view.statusButton(event.getaBoolean());
                break;

            case LoginEvent.showMessageSnack:
                view.showSnackBar(event.getMessage());
                break;

            case  LoginEvent.validarLogin:
                view.validarLogin(event.getaBoolean());
                break;

        }

    }

    @Override
    public void sendLogin(String _email, String _pass) {
        interactor.sendLogin(_email,_pass);
    }
}
