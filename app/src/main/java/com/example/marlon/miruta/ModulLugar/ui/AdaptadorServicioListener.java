package com.example.marlon.miruta.ModulLugar.ui;

import com.example.marlon.miruta.DataModel.DetalleLugar;

public interface AdaptadorServicioListener {
    void selectServicio(DetalleLugar _detallelugar);
}
