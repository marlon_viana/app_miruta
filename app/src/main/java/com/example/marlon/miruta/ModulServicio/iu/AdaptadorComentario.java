package com.example.marlon.miruta.ModulServicio.iu;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.marlon.miruta.DataModel.Comentario;
import com.example.marlon.miruta.ModulLugar.ui.AdaptadorServicioListener;
import com.example.marlon.miruta.R;

import java.util.List;

public class AdaptadorComentario extends RecyclerView.Adapter<AdaptadorComentario.ViewHolderData> {

    private List<Comentario> items;
    private String idusuario;
    private AdaptadorComentarioListener listener;

    public AdaptadorComentario(List<Comentario> items,String idusuario,AdaptadorComentarioListener listener) {
        this.items = items;
        this.idusuario= idusuario;
        this.listener= listener;
    }

    public class ViewHolderData extends RecyclerView.ViewHolder {

        TextView textViewComentario,textViewFecha;
        ImageButton imageButtonDelet;

        public ViewHolderData(View itemView) {
            super(itemView);
            textViewComentario= itemView.findViewById(R.id.textview_comentario);
            textViewFecha= itemView.findViewById(R.id.textview_fechacomentario);
            imageButtonDelet= itemView.findViewById(R.id.button_delet_message);
        }
    }

    @NonNull
    @Override
    public AdaptadorComentario.ViewHolderData onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_cardcomentario,viewGroup,false);
        return new AdaptadorComentario.ViewHolderData(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorComentario.ViewHolderData viewHolderData, final int i) {

        viewHolderData.textViewComentario.setText(items.get(i).getUSCCOMENTARIO());
        viewHolderData.textViewFecha.setText(items.get(i).getUSCFECHACOMENTARIO());

        if (idusuario.equals(items.get(i).getIDUSUARIOS())){
            viewHolderData.imageButtonDelet.setVisibility(View.VISIBLE);
        }else {
            viewHolderData.imageButtonDelet.setVisibility(View.GONE);
        }

        viewHolderData.imageButtonDelet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.borrarComentario(items.get(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
