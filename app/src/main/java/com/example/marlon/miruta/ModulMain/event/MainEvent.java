package com.example.marlon.miruta.ModulMain.event;

import com.example.marlon.miruta.DataModel.User;

import java.util.List;

public class MainEvent {
    public final static int infoHeader=0;

    private List list;
    private int eventType;
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

}
