package com.example.marlon.miruta.ModulRuta.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.marlon.miruta.DataModel.Ruta;
import com.example.marlon.miruta.R;

import java.util.List;

public class AdaptadorRutas extends RecyclerView.Adapter<AdaptadorRutas.ViewHolderData>{

    private AdaptadorRutasListener listener;
    private List<Ruta> items;

    public AdaptadorRutas(List<Ruta> items, AdaptadorRutasListener listener) {
        this.items = items;
        this.listener= listener;
    }

    public class ViewHolderData extends RecyclerView.ViewHolder {

        TextView textViewTitle,textViewSubTitle;
        ImageButton buttonShare,buttonDelet;

        public ViewHolderData(View itemView) {
            super(itemView);
            textViewTitle= itemView.findViewById(R.id.textview_tituloruta);
            textViewSubTitle= itemView.findViewById(R.id.textview_descripcionruta);
            buttonDelet= itemView.findViewById(R.id.button_delet_ruta);
            buttonShare= itemView.findViewById(R.id.button_share);
        }
    }

    @NonNull
    @Override
    public ViewHolderData onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_cardubicacion,viewGroup,false);

        return new ViewHolderData(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderData viewHolderData, final int i) {
        viewHolderData.textViewTitle.setText(items.get(i).getRTNOMBRE());
        viewHolderData.textViewSubTitle.setText(items.get(i).getRTDESCRIPCION());

        viewHolderData.buttonDelet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.deletRuta(items.get(i));
            }
        });

        viewHolderData.buttonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.shareRuta(items.get(i));
            }
        });

        viewHolderData.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.selctRuta(items.get(i));
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}
