package com.example.marlon.miruta.ModulServicio.iu;

import com.example.marlon.miruta.DataModel.Comentario;

public interface AdaptadorComentarioListener {
    void borrarComentario(Comentario _comentario);
}
