package com.example.marlon.miruta.ModulLogin.di;

import com.example.marlon.miruta.Services.ContextModule;
import com.example.marlon.miruta.Services.PreferencesModule;
import com.example.marlon.miruta.Services.ServiceRequestModule;
import com.example.marlon.miruta.lib.di.LibsModule;
import com.example.marlon.miruta.ModulLogin.ui.LoginActivity;

import javax.inject.Singleton;
import dagger.Component;

@Singleton
@Component(modules = {LibsModule.class, LoginModule.class, ContextModule.class, ServiceRequestModule.class, PreferencesModule.class})
public interface LoginComponent {
    void inject(LoginActivity activity);
}
