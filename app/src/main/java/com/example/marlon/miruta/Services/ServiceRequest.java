package com.example.marlon.miruta.Services;

import com.example.marlon.miruta.DataModel.Calificacion;
import com.example.marlon.miruta.DataModel.Comentario;
import com.example.marlon.miruta.DataModel.Message;
import com.example.marlon.miruta.DataModel.Nodo;
import com.example.marlon.miruta.DataModel.Ruta;
import com.example.marlon.miruta.DataModel.User;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ServiceRequest {

   @POST("login")
   @FormUrlEncoded
   Call<User> login(@Field("email") String user, @Field("password") String pass);

   @GET("user/{idruta}/ruta")
   Call<List<Ruta>> listRutaUsuario(@Path("idruta") String user);

   @DELETE("user/{iduser}/ruta/{idruta}")
   Call<Message> borrarRuta(@Path("iduser") String user,@Path("idruta") String ruta);

   @POST("ruta")
   @FormUrlEncoded
   Call<List<Nodo>> nodos(@Field("id_ruta") String id);

   @GET("comentarios/create")
   Call <Message> comentario(@Query("ID_USUARIOS") String iduser,
                             @Query("ID_DETALLE_LT_S") String iddetalle,
                             @Query("USC_TITULO") String titulo,
                             @Query("USC_COMENTARIO") String comentario);

   @POST("calificar")
   @FormUrlEncoded
   Call <Message> calificacion(@Field("ID_USUARIOS") String iduser,
                              @Field("ID_DETALLE_LT_S") String iddetalle,
                              @Field("ID_CALIFICACION") String idcalif,
                              @Field("ID_RUTA")String idruta);


   @GET("comentarios/{id_detalle}")
   Call<List<Comentario>> obtenercomentarios(@Path("id_detalle") String iddetelle);

   @DELETE("comentarios/{id_comentario}")
   Call<Message> borrarComentario(@Path("id_comentario") String idcomentario);

   @GET("calificacion")
   Call <Calificacion> obtenercalifiacion(@Query("ID_RUTA")String idruta,
                                          @Query("ID_DETALLE_LT_S") String iddetalle);

   @PUT("user/{iduser}")
   @FormUrlEncoded
   Call<User> actualizarusuario(@Path("iduser") String user,
                                @Field("U_ALIAS")String alias,
                                @Field("U_NOMBRE")String nombre,
                                @Field("U_APELLIDO")String apellido,
                                @Field("U_EMAIL")String email,
                                @Field("U_DNI")String dni,
                                @Field("U_FECHA_NACIMIENTO")String fecha,
                                @Field("U_SEXO")String sexo,
                                @Field("U_DIRECCION")String direccion,
                                @Field("U_TELEFONO")String telefono,
                                @Field("U_FOTO")String foto);

   @Multipart
   @POST("upload")
   Call<User> postImage(@Part MultipartBody.Part image
                        ,@Part("ID_USUARIO") RequestBody iduser);

}
