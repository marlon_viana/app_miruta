package com.example.marlon.miruta.ModulServicio;

import com.example.marlon.miruta.ModulServicio.event.ServicioEvent;

public interface ServicioPressenter {
    void onCreate();
    void onDestroy();
    void onEventMainThread(ServicioEvent event);
    void sendCalificacion(String _iddetalle,String _idcalifacion,String _idruta);
    void sendComentario(String _iddetalle,String _titulo,String _comentario);
    void getValoracionComentarios(String _iddetalle,String _idruta);
    void borrarComentario(String _idComentario);
}
