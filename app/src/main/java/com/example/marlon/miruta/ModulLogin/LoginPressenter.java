package com.example.marlon.miruta.ModulLogin;
import com.example.marlon.miruta.ModulLogin.event.LoginEvent;


public interface LoginPressenter  {
    void onCreate();
    void onDestroy();
    void onEventMainThread(LoginEvent event);
    void sendLogin(String _email,String _pass);
}
