package com.example.marlon.miruta.ModulMaps.iu;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.marlon.miruta.DataModel.LugarTuristico;
import com.example.marlon.miruta.DataModel.Nodo;
import com.example.marlon.miruta.DataModel.Ruta;
import com.example.marlon.miruta.MobileApplication;
import com.example.marlon.miruta.ModulLugar.ui.LugarActivity;
import com.example.marlon.miruta.ModulMain.ui.MainActivity;
import com.example.marlon.miruta.ModulMaps.MapsPressenter;
import com.example.marlon.miruta.ModulMaps.di.MapsComponent;
import com.example.marlon.miruta.ModulRuta.ui.AdaptadorRutas;
import com.example.marlon.miruta.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MapaActivity extends AppCompatActivity implements OnMapReadyCallback,MapsView
        ,GoogleMap.OnMarkerClickListener,View.OnClickListener, AdaptadorLugarListener {

    private TextView titeltoolbar,textViewInfo,textViewInfoMarketTitle,textViewInfoMarketDescrip;
    private Ruta ruta;
    private GoogleMap mMap;
    private ConstraintLayout constraintLayoutrutainfo,constraintLayoutInfomarket,constraintLayoutContext;
    private FloatingActionButton floatingButton;
    private List<Nodo> listnodo;
    private List<LugarTuristico> listlugar;

    private RecyclerView recyclerViewLugares;

    
    @Inject
    MapsPressenter pressenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        getBundle();
        addView();
        addToolBar();
        setupInject();
        pressenter.onCreate();
        pressenter.getNodos(ruta.getIDRUTA());
    }

    private void setupInject() {
        MobileApplication application = (MobileApplication) getApplication();
        MapsComponent component = application.getMapsComponent(this);
        component.inject(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pressenter.onDestroy();
    }

    private void addView() {
        constraintLayoutrutainfo= findViewById(R.id.contenidoUbicacion);
        textViewInfo= findViewById(R.id.textview_infoRutas);

        constraintLayoutInfomarket=findViewById(R.id.infomarket_view);
        constraintLayoutInfomarket.setVisibility(View.GONE);
        floatingButton=findViewById(R.id.floatingActionButtonCalificacion);
        floatingButton.setOnClickListener(this);

        textViewInfoMarketTitle=findViewById(R.id.text_tituloNodo);
        textViewInfoMarketDescrip=findViewById(R.id.text_descripcionNodo);

        recyclerViewLugares=findViewById(R.id.recyclerViewLugares);
        recyclerViewLugares.setVisibility(View.GONE);


        constraintLayoutContext= findViewById(R.id.constraintLayoutMapa);

        titeltoolbar= findViewById(R.id.textview_titulotoolbar);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                                        .findFragmentById(R.id.fragmentmapa);
        mapFragment.getMapAsync(this);
    }

    private void addToolBar() {
        Toolbar toolbar=findViewById(R.id.toolbar_mapa);
        toolbar.setTitle("");
        titeltoolbar.setText(ruta.getRTNOMBRE());
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // <- Aquí :)
                startActivity(intent);
                finish();
            }
        });
    }

    private void getBundle(){
        String rutaBundle= getIntent().getExtras().getString("RutaBundle");
        Gson gson= new Gson();
        ruta= gson.fromJson(rutaBundle,Ruta.class);
    }

    @Override
    public void showLayoutMapa(boolean _boolean) {
        if (_boolean){
            constraintLayoutrutainfo.setVisibility(View.VISIBLE);
        }else {
            constraintLayoutrutainfo.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void showMessageLayout(String _message) {
        textViewInfo.setText(_message);
    }

    public void showLugares(boolean _boolean){
        if (_boolean){
            recyclerViewLugares.setVisibility(View.VISIBLE);
            recyclerViewLugares.setLayoutManager(new LinearLayoutManager(this,
                    LinearLayoutManager.VERTICAL,false));
            AdaptadorLugar adapter= new AdaptadorLugar(listlugar,this);
            recyclerViewLugares.setAdapter(adapter);

            LayoutAnimationController controller=AnimationUtils.loadLayoutAnimation
                    (recyclerViewLugares.getContext(),R.anim.animlt_lefttoright);
            recyclerViewLugares.setLayoutAnimation(controller);
            recyclerViewLugares.getAdapter().notifyDataSetChanged();
            recyclerViewLugares.scheduleLayoutAnimation();
        }else {
            recyclerViewLugares.setVisibility(View.GONE);
        }
    }

    @Override
    public void showInfoMarket(Nodo _nodo) {
        constraintLayoutInfomarket.setVisibility(View.VISIBLE);
        LayoutAnimationController controller= AnimationUtils.loadLayoutAnimation
                                    (constraintLayoutInfomarket.getContext(),R.anim.animlt_up);
        constraintLayoutInfomarket.setLayoutAnimation(controller);
        constraintLayoutInfomarket.scheduleLayoutAnimation();
        textViewInfoMarketTitle.setText(_nodo.getNONOMBRE());
        textViewInfoMarketDescrip.setText(_nodo.getNODIRECCION());
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.floatingActionButtonCalificacion:
                if (listlugar.size()==1){
                    selectLugar(listlugar.get(0));
                }else if (listlugar.size()>1){
                    if (recyclerViewLugares.getVisibility()==View.GONE){
                        showLugares(true);
                    }else {
                        showLugares(false);
                    }
                }
                break;

        }

    }

    @Override
    public void selectLugar(LugarTuristico lugarTuristico) {
        Intent lugarIntent= new Intent(this,LugarActivity.class);
        lugarIntent.putExtra("RutaBundle",ruta.makeJson());
        lugarIntent.putExtra("LugarBundle",lugarTuristico.makeJson());
        startActivity(lugarIntent);
    }

    /////*****************************************MAPS*****************************************/////

    //Instancia el mapa de google
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        try {
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.stilomapa));

            if (!success) {
                Log.e("ERROS STILO MAPA", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("ERRO STILO MAPA", "Can't find style. Error: ", e);
        }
    }

    //Recorre la lista de nodos y coloca la camara en el primer nodo
    @Override
    public void showNodos(List _list) {

        listnodo= _list;

        for (int i=0; i< listnodo.size();i++){
            Nodo nodo= listnodo.get(i);
            Double latitud= Double.parseDouble(nodo.getNOLATITUD());
            Double longitud= Double.parseDouble(nodo.getNOLONGITUD());
            String title= nodo.getNONOMBRE();
            LatLng lagLon = new LatLng(latitud, longitud);
            addMarkect(lagLon,title);

            if (i==0){
                moveCamera(lagLon);
            }
        }

    }

    //Coloca los mapas en el nodo
    public void addMarkect(LatLng latLng, String titulo){
        mMap.setOnMarkerClickListener(this);
        mMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .title(titulo))
                            .setIcon(BitmapDescriptorFactory.fromBitmap(resizeIconMarket()));
    }

    //Reajusta el diseño del market
    public Bitmap resizeIconMarket(){
        int height = 100;
        int width = 100;
        BitmapDrawable bitmapdraw= new BitmapDrawable();
        bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.img_pt_rojo);
        Bitmap b=bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        return smallMarker;
    }

    //Animacion de la camara en el mapa
    public void moveCamera(LatLng latLng){
        CameraUpdate camarausu=CameraUpdateFactory.newLatLngZoom(latLng,17);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(camarausu);
    }

    //Listener al hacer click en un market
    @Override
    public boolean onMarkerClick(Marker marker) {
        lookingNodo(marker.getTitle());
        return false;
    }

    //Recorre la lista de nodos para obener el nodo relacion con el market seleccionado
    private void lookingNodo(String _title){
        if(listnodo!=null){
            for (Nodo nodo: listnodo) {
                if(nodo.getNONOMBRE().equals(_title)){
                    showInfoMarket(nodo);
                    int nLugares= nodo.getLUGARESTURISTICOS().size();
                    listlugar= new ArrayList<>();
                    for (int j=0;j<nLugares;j++){
                        listlugar.add(nodo.getLUGARESTURISTICOS().get(j));
                    }
                }
            }
        }
    }

    //Muestra las PolyLine en el mapa
    @Override
    public void showRutas(List _list) {

        List<LatLng> list = _list;
        mMap.addPolyline(new PolylineOptions()
                .addAll(list)
                .width(20)
                .color(getResources().getColor(R.color.colorazulmar))
                .geodesic(true));
    }


}
