package com.example.marlon.miruta.ModulLogin;

import android.content.Context;
import android.util.Log;

import com.example.marlon.miruta.DataModel.User;
import com.example.marlon.miruta.ModulLogin.event.LoginEvent;
import com.example.marlon.miruta.R;
import com.example.marlon.miruta.Services.PreferencesManager;
import com.example.marlon.miruta.Services.ServiceConectivity;
import com.example.marlon.miruta.Services.ServiceRequest;

import org.greenrobot.eventbus.EventBus;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginInteractorImpl implements LoginInteractor {

    private EventBus eventBus;
    private ServiceRequest request;
    private PreferencesManager preferences;
    private Context context;

    public LoginInteractorImpl(EventBus eventBus, ServiceRequest request, PreferencesManager preferences, Context context) {
        this.eventBus = eventBus;
        this.request = request;
        this.preferences = preferences;
        this.context= context;
    }

    //Consula el email y pass con el api para hacer login
    @Override
    public void sendLogin(String _email, String _pass) {

        if (ValidarEmail(_email)){
            if (ServiceConectivity.isNetDisponible(context)){
                booleanEvent(false,LoginEvent.statusButton);

                request.login(_email, _pass).enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        booleanEvent(true,LoginEvent.statusButton);
                        try {
                            User user= response.body();
                            if (!user.getUEMAIL().isEmpty()){
                                preferences.setUser(user);
                                preferences.logout(true);
                                postEvent(LoginEvent.nextActivity);
                            }else {
                                messageEvent(context.getResources().getString(R.string.error_login)
                                        ,LoginEvent.showMessageSnack);
                            }
                        }catch (Exception e){
                            Log.e("ERROR",e.toString());
                            messageEvent(context.getResources().getString(R.string.error_login)
                                        ,LoginEvent.showMessageSnack);
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        booleanEvent(true,LoginEvent.statusButton);
                        messageEvent(context.getResources().getString(R.string.no_api)
                                    ,LoginEvent.showMessageSnack);
                    }
                });

            }else {
                messageEvent(context.getResources().getString(R.string.no_internet)
                            ,LoginEvent.showMessageSnack);
            }
        }
    }

    //Valida que usuari si el usuario ya inicio sesion
    @Override
    public void validarLogin() {
        booleanEvent(preferences.isLogged(),LoginEvent.validarLogin);
    }



    //Valida que el String de email sea un email valido
    private Boolean ValidarEmail(String _email){
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

        Matcher mather = pattern.matcher(_email);

        if (mather.find() == true) {
            return  true;
        } else {
            messageEvent(context.getResources().getString(R.string.error_email),LoginEvent.showMessage);
            return  false;
        }
    }


    //EVENTOS
    private void postEvent(int type){
        LoginEvent event= new LoginEvent();
        event.setEventType(type);
        eventBus.postSticky(event);
    }

    private void messageEvent(String message,int type){
        LoginEvent event= new LoginEvent();
        event.setEventType(type);
        event.setMessage(message);
        eventBus.postSticky(event);
    }

    private void booleanEvent(boolean ban,int type){
        LoginEvent event= new LoginEvent();
        event.setEventType(type);
        event.setaBoolean(ban);
        eventBus.postSticky(event);
    }
}
