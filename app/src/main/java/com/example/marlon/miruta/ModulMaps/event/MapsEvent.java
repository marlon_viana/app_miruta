package com.example.marlon.miruta.ModulMaps.event;

import java.util.List;

public class MapsEvent {

    public final static int showMessageInfo= 0;
    public final static int showLayoutMaps=1;
    public final static int showNodos=2;
    public final static int showPoly=3;


    private String Message;
    private int eventType;
    private Boolean aBoolean;
    private List list;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public Boolean getaBoolean() {
        return aBoolean;
    }

    public void setaBoolean(Boolean aBoolean) {
        this.aBoolean = aBoolean;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }
}
