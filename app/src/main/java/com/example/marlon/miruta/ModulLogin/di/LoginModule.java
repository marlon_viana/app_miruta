package com.example.marlon.miruta.ModulLogin.di;


import android.content.Context;

import com.example.marlon.miruta.Services.PreferencesManager;
import com.example.marlon.miruta.Services.ServiceRequest;
import com.example.marlon.miruta.ModulLogin.LoginInteractor;
import com.example.marlon.miruta.ModulLogin.LoginInteractorImpl;
import com.example.marlon.miruta.ModulLogin.LoginPressenter;
import com.example.marlon.miruta.ModulLogin.LoginPressenterImpl;
import com.example.marlon.miruta.ModulLogin.ui.LoginView;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {
    private LoginView view;

    public LoginModule(LoginView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    LoginPressenter providerLoginPressenter(EventBus eventBus, LoginView view,LoginInteractor  interactor){
        return new LoginPressenterImpl(eventBus, view, interactor);
    }
    @Provides
    @Singleton
    LoginInteractor providerLoginInteractor(EventBus eventBus, ServiceRequest request, PreferencesManager preferences, Context context){
        return new LoginInteractorImpl(eventBus, request, preferences, context);
    }

    @Provides
    @Singleton
    LoginView providerLoginView(){
        return this.view;
    }
}
