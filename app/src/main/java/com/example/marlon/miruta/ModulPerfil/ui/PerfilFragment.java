package com.example.marlon.miruta.ModulPerfil.ui;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.marlon.miruta.DataModel.User;
import com.example.marlon.miruta.MobileApplication;
import com.example.marlon.miruta.ModulPerfil.PerfilPressenter;
import com.example.marlon.miruta.ModulPerfil.di.PerfilComponent;
import com.example.marlon.miruta.R;
import com.example.marlon.miruta.Services.CircleTransformService;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;

public class PerfilFragment extends Fragment implements PerfilView,View.OnClickListener {


    private View view;
    private EditText textViewAlias,textViewNombre,textViewTelefono,textViewEmail;
    private ImageView imageViewUsuario;
    private ImageButton imageButtonEdit;

    private Boolean aBooleanEdit=false;
    private ConstraintLayout constraintLayoutBody;
    private FloatingActionMenu floatingActionMenu;
    private FloatingActionButton floatingCamara,floatingGaleria,floatingUp;
    private Animation fadein,fadeout;
    private ContentValues cv;
    private final int SELECT_PICTURE = 300;
    private final int PHOTO_CODE = 200;
    private Uri imageUri;

    private Uri path=null;
    private Bitmap BitmapMaster;
    private File mySelectedFile;
    private ListenerPerfilUsuario listener;


    @Inject
    PerfilPressenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view= inflater.inflate(R.layout.fragmentlayout_usuarioperfil, container, false);
        addView();
        editUser();
        setupInject();
        presenter.onCreate();
        return view;
    }

    @SuppressLint("ValidFragment")
    public PerfilFragment(ListenerPerfilUsuario listener){
        this.listener= listener;
    }

    private void setupInject() {
        MobileApplication application = (MobileApplication) getActivity().getApplication();
        PerfilComponent component = application.getPerfilComponent(this);
        component.inject(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    private void addView(){
        fadein= AnimationUtils.loadAnimation(view.getContext(),R.anim.fade_in);
        fadeout= AnimationUtils.loadAnimation(view.getContext(),R.anim.fade_out);

        constraintLayoutBody= view.findViewById(R.id.cuerpo_usuario);
        LayoutAnimationController controller= AnimationUtils.loadLayoutAnimation
                (constraintLayoutBody.getContext(),R.anim.animlt_up);
        constraintLayoutBody.setLayoutAnimation(controller);
        constraintLayoutBody.scheduleLayoutAnimation();

        imageButtonEdit= view.findViewById(R.id.button_edituser);
        imageButtonEdit.setOnClickListener(this);

        floatingActionMenu= view.findViewById(R.id.floating_action_menu);
        floatingActionMenu.setVisibility(View.GONE);
        floatingCamara=view.findViewById(R.id.floating_camara);
        floatingCamara.setOnClickListener(this);
        floatingGaleria= view.findViewById(R.id.floating_galeria);
        floatingGaleria.setOnClickListener(this);
        floatingUp= view.findViewById(R.id.floating_subir);
        floatingUp.setOnClickListener(this);

        textViewAlias= view.findViewById(R.id.textview_alias);
        textViewNombre=view.findViewById(R.id.textview_nombre);
        textViewTelefono=view.findViewById(R.id.textview_telefono);
        textViewEmail=view.findViewById(R.id.textview_email);
        imageViewUsuario= view.findViewById(R.id.imageView_User);
    }

    @Override
    public void showInfoPerfil(User _user) {
       Picasso.get()
               .load(getResources().getString(R.string.url_base_foto)+_user.getUFOTO())
               .transform(new CircleTransformService())
               .into(imageViewUsuario);

        if (_user.getUALIAS()!=null){
            textViewAlias.setText(_user.getUALIAS().toString());
        }

        if (_user.getUNOMBRE()!=null){
            textViewNombre.setText(_user.getUNOMBRE());
        }
        if (_user.getUTELEFONO()!= null){
            textViewTelefono.setText(_user.getUTELEFONO());
        }
        if (_user.getUEMAIL()!=null){
            textViewEmail.setText(_user.getUEMAIL());
        }
    }

    @Override
    public void statusButtonUpload(Boolean _ban) {
        floatingUp.setEnabled(_ban);
    }

    @Override
    public void showSnakBar(String _message) {
        Snackbar mSnackBar = Snackbar.make(view, _message, Snackbar.LENGTH_LONG);
        TextView mainTextView =(mSnackBar.getView()).findViewById(android.support.design.R.id.snackbar_text);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            mainTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }else{
            mainTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        mainTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        mainTextView.setTextColor(getResources().getColor(R.color.colorblanco));
        mSnackBar.show();
    }

    @Override
    public void cambiosRelizados() {
        listener.changesPerfil();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_edituser:
                if (!aBooleanEdit){
                    aBooleanEdit=true;
                }else {
                    aBooleanEdit=false;
                }
                editUser();
                break;

            case R.id.floating_camara:
                openCamera();
                break;

            case R.id.floating_galeria:
                openGallery();
                break;

            case R.id.floating_subir:
                   subirCambios();
                break;
        }
    }

    private void editUser(){

        if (aBooleanEdit){
            floatingActionMenu.startAnimation(fadein);
            floatingActionMenu.setVisibility(View.VISIBLE);

            textViewAlias.setFocusableInTouchMode(true);
            textViewNombre.setFocusableInTouchMode(true);
            textViewTelefono.setFocusableInTouchMode(true);
           // textViewEmail.setFocusableInTouchMode(true);

            textViewAlias.setEnabled(true);
            textViewNombre.setEnabled(true);
            textViewTelefono.setEnabled(true);
           // textViewEmail.setEnabled(true);
        }else {
            textViewAlias.setFocusable(false);
            textViewNombre.setFocusable(false);
            textViewTelefono.setFocusable(false);
            textViewEmail.setFocusable(false);

            textViewAlias.setEnabled(false);
            textViewNombre.setEnabled(false);
            textViewTelefono.setEnabled(false);
            textViewEmail.setEnabled(false);

            floatingActionMenu.close(true);

            floatingActionMenu.startAnimation(fadeout);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    floatingActionMenu.setVisibility(View.GONE);
                }
            }, 900);


        }
    }

    private void subirCambios(){
        String alias= textViewAlias.getText().toString().trim();
        String nombre= textViewNombre.getText().toString().trim();
        String telefono= textViewTelefono.getText().toString().trim();

        if (alias.isEmpty()||nombre.isEmpty()||telefono.isEmpty()){
            showSnakBar(getString(R.string.error_campos));
        }else {
            presenter.actualizarUsuario(alias,nombre,telefono);
        }

    }

    private void openCamera(){
        cv = new ContentValues();
        cv.put(MediaStore.Images.Media.TITLE, "Foto perfil");
        cv.put(MediaStore.Images.Media.DESCRIPTION, "From Camera");
        imageUri = view.getContext().getContentResolver()
                .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, cv);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, PHOTO_CODE);
    }

    private void  openGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media
                                    .EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent.createChooser(intent, getString(R.string.selectImageApp))
                                ,SELECT_PICTURE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            if(resultCode == getActivity().RESULT_OK){
            switch (requestCode){
                case PHOTO_CODE:
                    try {
                        BitmapMaster = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
                        path = getImageUri(view.getContext(), BitmapMaster);
                        redondeaImagen(BitmapMaster);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case SELECT_PICTURE:
                    path = data.getData();
                    InputStream imageStream=null;
                    try {
                        imageStream = getActivity().getContentResolver().openInputStream(path);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    BitmapMaster = BitmapFactory.decodeStream(imageStream);
                    redondeaImagen(BitmapMaster);
                    break;

                default:
                    showSnakBar(getActivity().getString(R.string.error_image));
                    break;
            }
        }
    }

    private void redondeaImagen(Bitmap _bitmap){
        RoundedBitmapDrawable roundedDrawable =
                RoundedBitmapDrawableFactory.create(getResources(),_bitmap);

        roundedDrawable.setCornerRadius(_bitmap.getHeight());
        showImagen(roundedDrawable);
    }

    public void showImagen(RoundedBitmapDrawable _bitmap) {
        imageViewUsuario.setImageDrawable(_bitmap);
        generateFile();
    }

    private Uri getImageUri(Context inContext, Bitmap inImage) {
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }

    //Genera el file
    private void generateFile(){
        String pathsel=getPathFromURI(path);

        if(pathsel==null){
            //ShowInfo(getString(R.string.PleaseUpload),false);
        }else {
            try {
                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(pathsel);
                    BitmapMaster.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            mySelectedFile = new File(pathsel);
            if (mySelectedFile.exists()){
                presenter.enviarImagen(mySelectedFile);
            }
        }

    }

    /// obtiene el patch de la uri
    public String getPathFromURI(Uri contentUri) {
        String res = null;

        String[] proj = {MediaStore.Images.Media.DATA};
        if(contentUri==null){
            Log.e("Error","Failed to load image");
        }else{
            Cursor cursor = getActivity().getContentResolver().query(contentUri, proj
                            , null, null, null);
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                res = cursor.getString(column_index);
            }
            cursor.close();
        }
        return res;
    }




}
