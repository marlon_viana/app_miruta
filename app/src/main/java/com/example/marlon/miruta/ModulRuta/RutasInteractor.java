package com.example.marlon.miruta.ModulRuta;

public interface RutasInteractor {
    void obtenerRutas();
    void borrarRuta(String _idRuta);
}
