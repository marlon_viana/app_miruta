package com.example.marlon.miruta.ModulMain;
import com.example.marlon.miruta.ModulMain.event.MainEvent;

public interface MainPressenter {
    void onCreate();
    void onDestroy();
    void onEventMainThread(MainEvent event);
    void logOut();
    void obtenerPerfilUsuario();
}
