package com.example.marlon.miruta.ModulPerfil.ui;

import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;

import com.example.marlon.miruta.DataModel.User;

import java.util.List;

public interface PerfilView {

    void showInfoPerfil(User _user);
    void statusButtonUpload(Boolean _ban);
    void showSnakBar(String _message);
    void cambiosRelizados();

}
