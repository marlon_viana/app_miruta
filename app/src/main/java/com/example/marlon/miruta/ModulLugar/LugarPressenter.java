package com.example.marlon.miruta.ModulLugar;

import com.example.marlon.miruta.ModulLugar.event.LugarEvent;

public interface LugarPressenter {
    void onCreate();
    void onDestroy();
    void onEventMainThread(LugarEvent event);
}
