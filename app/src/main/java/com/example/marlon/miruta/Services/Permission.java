package com.example.marlon.miruta.Services;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.example.marlon.miruta.ModulLogin.ui.LoginActivity;
import com.example.marlon.miruta.R;

public class Permission {

    public static final int EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_BY_LOAD_PROFILE = 2;
    public static final int CAMERA_PERMISSION_REQUEST_CODE = 3;
    static Context mContext=null;
    static Activity mActivity=null;

    /***
     * Permissions
     * @param context
     */
    public Permission(LoginActivity context){
        this.mContext=context;
        this.mActivity=context;
    }


    public static Boolean AceptGroupPermissionStorage(){

            int result = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (result == PackageManager.PERMISSION_GRANTED){
                return true;
            } else {
                return false;
        }
    }

    public static boolean checkPermissionForCamera(){

        int result = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    public void requestPermissionForExternalStorage(int requestCode){
        ActivityCompat.requestPermissions(mActivity,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},requestCode);
        /*if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            Toast.makeText(mContext.getApplicationContext(),mActivity.getResources().getString(R.string.permiso_directorio), Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(mActivity,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},requestCode);
        }*/
    }

    public void requestPermissionForCamera(){
        ActivityCompat.requestPermissions(mActivity,new String[]{Manifest.permission.CAMERA},CAMERA_PERMISSION_REQUEST_CODE);

        /*if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.CAMERA)){
            Toast.makeText(mContext.getApplicationContext(),mActivity.getResources().getString(R.string.permiso_camara), Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(mActivity,new String[]{Manifest.permission.CAMERA},CAMERA_PERMISSION_REQUEST_CODE);
        }*/
    }

}
