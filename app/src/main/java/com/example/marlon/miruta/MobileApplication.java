package com.example.marlon.miruta;

import android.app.Application;

import com.example.marlon.miruta.ModulLugar.di.DaggerLugarComponent;
import com.example.marlon.miruta.ModulLugar.di.LugarComponent;
import com.example.marlon.miruta.ModulLugar.di.LugarModule;
import com.example.marlon.miruta.ModulLugar.ui.LugarView;
import com.example.marlon.miruta.ModulMain.di.DaggerMainComponent;
import com.example.marlon.miruta.ModulMain.di.MainComponent;
import com.example.marlon.miruta.ModulMain.di.MainModule;
import com.example.marlon.miruta.ModulMain.ui.MainView;
import com.example.marlon.miruta.ModulMaps.di.DaggerMapsComponent;
import com.example.marlon.miruta.ModulMaps.di.MapsComponent;
import com.example.marlon.miruta.ModulMaps.di.MapsModule;
import com.example.marlon.miruta.ModulMaps.iu.MapsView;
import com.example.marlon.miruta.ModulPerfil.di.DaggerPerfilComponent;
import com.example.marlon.miruta.ModulPerfil.di.PerfilComponent;
import com.example.marlon.miruta.ModulPerfil.di.PerfilModule;
import com.example.marlon.miruta.ModulPerfil.ui.PerfilView;
import com.example.marlon.miruta.ModulRuta.di.DaggerRutasComponent;
import com.example.marlon.miruta.ModulRuta.di.RutasComponent;
import com.example.marlon.miruta.ModulRuta.di.RutasModule;
import com.example.marlon.miruta.ModulRuta.ui.RutasView;
import com.example.marlon.miruta.ModulServicio.di.DaggerServicioComponent;
import com.example.marlon.miruta.ModulServicio.di.ServicioComponent;
import com.example.marlon.miruta.ModulServicio.di.ServicioModule;
import com.example.marlon.miruta.ModulServicio.iu.ServicioView;
import com.example.marlon.miruta.Services.ContextModule;
import com.example.marlon.miruta.Services.ServiceRequestModule;
import com.example.marlon.miruta.lib.di.LibsModule;
import com.example.marlon.miruta.ModulLogin.di.DaggerLoginComponent;
import com.example.marlon.miruta.ModulLogin.di.LoginComponent;
import com.example.marlon.miruta.ModulLogin.di.LoginModule;
import com.example.marlon.miruta.ModulLogin.ui.LoginView;

public class MobileApplication extends Application{
    private static final String URL_BASE="http://preuniversitarioaprender.com/ups_servicios/public/";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public LoginComponent getLoginComponent(LoginView view){
        return DaggerLoginComponent
                .builder()
                .libsModule(new LibsModule())
                .loginModule(new LoginModule(view))
                .contextModule(new ContextModule(getApplicationContext()))
                .serviceRequestModule(new ServiceRequestModule(URL_BASE))
                .build();
    }

    public PerfilComponent getPerfilComponent(PerfilView view){

        return DaggerPerfilComponent
                .builder()
                .libsModule(new LibsModule())
                .perfilModule(new PerfilModule(view))
                .contextModule(new ContextModule(getApplicationContext()))
                .serviceRequestModule(new ServiceRequestModule(URL_BASE))
                .build();
    }

    public MainComponent getMainComponent(MainView view){

        return DaggerMainComponent
                .builder()
                .libsModule(new LibsModule())
                .mainModule(new MainModule(view))
                .contextModule(new ContextModule(getApplicationContext()))
                .serviceRequestModule(new ServiceRequestModule(URL_BASE))
                .build();
    }

    public RutasComponent getRutasComponent(RutasView view){
        return DaggerRutasComponent
                .builder()
                .libsModule(new LibsModule())
                .rutasModule(new RutasModule(view))
                .contextModule(new ContextModule(getApplicationContext()))
                .serviceRequestModule(new ServiceRequestModule(URL_BASE))
                .build();
    }

    public MapsComponent getMapsComponent(MapsView view){
        return DaggerMapsComponent
                .builder()
                .libsModule(new LibsModule())
                .mapsModule(new MapsModule(view))
                .contextModule(new ContextModule(getApplicationContext()))
                .serviceRequestModule(new ServiceRequestModule(URL_BASE))
                .build();

    }

    public LugarComponent getLugarcomponent(LugarView view){
        return DaggerLugarComponent
                .builder()
                .libsModule(new LibsModule())
                .lugarModule(new LugarModule(view))
                .contextModule(new ContextModule(getApplicationContext()))
                .serviceRequestModule(new ServiceRequestModule(URL_BASE))
                .build();
    }

    public ServicioComponent getServicioComponent(ServicioView view){
        return DaggerServicioComponent
                .builder()
                .libsModule(new LibsModule())
                .servicioModule(new ServicioModule(view))
                .contextModule(new ContextModule(getApplicationContext()))
                .serviceRequestModule(new ServiceRequestModule(URL_BASE))
                .build();

    }


}

