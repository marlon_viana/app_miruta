package com.example.marlon.miruta.DataModel;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetalleLugar {
    @SerializedName("ID_DETALLE_LT_S")
    @Expose
    private String iDDETALLELTS;
    @SerializedName("ID_SERVICIO")
    @Expose
    private String iDSERVICIO;
    @SerializedName("DLTS_NOMBRE")
    @Expose
    private String dLTSNOMBRE;
    @SerializedName("DLTS_DETALLE")
    @Expose
    private String dLTSDETALLE;

    public String getIDDETALLELTS() {
        return iDDETALLELTS;
    }

    public void setIDDETALLELTS(String iDDETALLELTS) {
        this.iDDETALLELTS = iDDETALLELTS;
    }

    public String getIDSERVICIO() {
        return iDSERVICIO;
    }

    public void setIDSERVICIO(String iDSERVICIO) {
        this.iDSERVICIO = iDSERVICIO;
    }

    public String getDLTSNOMBRE() {
        return dLTSNOMBRE;
    }

    public void setDLTSNOMBRE(String dLTSNOMBRE) {
        this.dLTSNOMBRE = dLTSNOMBRE;
    }

    public String getDLTSDETALLE() {
        return dLTSDETALLE;
    }

    public void setDLTSDETALLE(String dLTSDETALLE) {
        this.dLTSDETALLE = dLTSDETALLE;
    }

    public String makeJson(){
        Gson gson= new Gson();
        String _jsonstring= gson.toJson(this);
        return  _jsonstring;
    }
}
