package com.example.marlon.miruta.DataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Calificacion {

    @SerializedName("promedio")
    @Expose
    private Double promedio;

    public Double getPromedio() {
        return promedio;
    }

    public void setPromedio(Double promedio) {
        this.promedio = promedio;
    }
}
