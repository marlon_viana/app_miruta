package com.example.marlon.miruta.ModulRuta;
import com.example.marlon.miruta.ModulRuta.event.RutasEvent;

public interface RutasPressenter {
    void onCreate();
    void onDestroy();
    void onEventMainThread(RutasEvent event);
    void borrarRuta(String _idRuta);
}
