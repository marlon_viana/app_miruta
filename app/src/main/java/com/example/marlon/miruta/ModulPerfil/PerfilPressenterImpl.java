package com.example.marlon.miruta.ModulPerfil;
import android.app.Activity;
import android.net.Uri;

import com.example.marlon.miruta.ModulPerfil.event.PerfilEvent;
import com.example.marlon.miruta.ModulPerfil.ui.PerfilView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.nio.file.Path;

public class PerfilPressenterImpl implements PerfilPressenter {


    private EventBus eventBus;
    private PerfilView view;
    private PerfilInteractor interactor;

    public PerfilPressenterImpl(EventBus eventBus, PerfilView view, PerfilInteractor interactor) {
        this.eventBus = eventBus;
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
        interactor.obtenerPerfilUsuario();
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }


    @Subscribe
    @Override
    public void onEventMainThread(PerfilEvent event) {

        switch (event.getEventType()) {
            case PerfilEvent.showPerfilUsuario:
                view.showInfoPerfil(event.getUser());
                break;

            case  PerfilEvent.statusButton:
                view.statusButtonUpload(event.getaBoolean());
                break;

            case PerfilEvent.showSnakBar:
                view.showSnakBar(event.getMessage());
                break;

            case PerfilEvent.showCambios:
                view.cambiosRelizados();
                break;
        }
    }

    @Override
    public void actualizarUsuario(String _alia, String _nombre, String _telefono) {
        interactor.actualizarUsuario(_alia,_nombre,_telefono);
    }

    @Override
    public void enviarImagen(File _file) {
        interactor.enviarImagen(_file);
    }

}
