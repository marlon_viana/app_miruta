package com.example.marlon.miruta.ModulServicio;

public interface ServicioInteractor {
    void sendCalificacion(String _iddetalle,String _idcalifacion,String _idruta);
    void sendComentario(String _iddetalle,String _titulo,String _comentario);
    void getValoracionComentarios(String _iddetalle, String _idruta);
    void deletComentario(String _idcomentario);
}
