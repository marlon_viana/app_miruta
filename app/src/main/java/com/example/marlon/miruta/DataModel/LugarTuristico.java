package com.example.marlon.miruta.DataModel;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LugarTuristico {
    @SerializedName("ID_LUGARESTURISTICOS")
    @Expose
    private String iDLUGARESTURISTICOS;
    @SerializedName("LT_NOMBRE")
    @Expose
    private String lTNOMBRE;
    @SerializedName("LT_DESCRIPCION")
    @Expose
    private String lTDESCRIPCION;
    @SerializedName("DETALLES_LTS")
    @Expose
    private List<DetalleLugar> dETALLESLTS = null;

    public String getIDLUGARESTURISTICOS() {
        return iDLUGARESTURISTICOS;
    }

    public void setIDLUGARESTURISTICOS(String iDLUGARESTURISTICOS) {
        this.iDLUGARESTURISTICOS = iDLUGARESTURISTICOS;
    }

    public String getLTNOMBRE() {
        return lTNOMBRE;
    }

    public void setLTNOMBRE(String lTNOMBRE) {
        this.lTNOMBRE = lTNOMBRE;
    }

    public String getLTDESCRIPCION() {
        return lTDESCRIPCION;
    }

    public void setLTDESCRIPCION(String lTDESCRIPCION) {
        this.lTDESCRIPCION = lTDESCRIPCION;
    }

    public List<DetalleLugar> getDETALLESLTS() {
        return dETALLESLTS;
    }

    public void setDETALLESLTS(List<DetalleLugar> dETALLESLTS) {
        this.dETALLESLTS = dETALLESLTS;
    }

    public String makeJson(){
        Gson gson= new Gson();
        String _jsonstring= gson.toJson(this);
        return  _jsonstring;
    }

}
