package com.example.marlon.miruta.ModulLugar.event;

public class LugarEvent {
    public final static int showSnack= 0;

    private String Message;
    private int eventType;

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
