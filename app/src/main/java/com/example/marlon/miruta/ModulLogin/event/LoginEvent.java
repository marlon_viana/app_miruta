package com.example.marlon.miruta.ModulLogin.event;

public class LoginEvent {

    public final static int statusButton = 0;
    public final static int showMessage = 1;
    public final static int nextActivity=2;
    public final static int showMessageSnack=3;
    public final static int validarLogin=4;

    private int eventType;
    private String Message;
    private boolean aBoolean;

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String sendMessage) {
        this.Message = sendMessage;
    }

    public boolean getaBoolean() {
        return aBoolean;
    }

    public void setaBoolean(boolean aBoolean) {
        this.aBoolean = aBoolean;
    }
}
