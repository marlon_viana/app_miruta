package com.example.marlon.miruta.ModulMaps.iu;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.marlon.miruta.DataModel.LugarTuristico;
import com.example.marlon.miruta.R;

import java.util.List;

class AdaptadorLugar extends RecyclerView.Adapter<AdaptadorLugar.ViewHolderData>{

    private AdaptadorLugarListener listener;
    private List<LugarTuristico> items;

    public AdaptadorLugar (List<LugarTuristico> items, AdaptadorLugarListener listener) {
        this.items = items;
        this.listener= listener;
    }

    public class ViewHolderData extends RecyclerView.ViewHolder {

        TextView textViewTitle;

        public ViewHolderData(View itemView) {
            super(itemView);
            textViewTitle= itemView.findViewById(R.id.textview_titulolugar);
        }
    }

    @NonNull
    @Override
    public AdaptadorLugar.ViewHolderData onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_cardlugares,viewGroup,false);

        return new ViewHolderData(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorLugar.ViewHolderData viewHolderData, final int i) {
        viewHolderData.textViewTitle.setText(items.get(i).getLTNOMBRE());

        viewHolderData.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.selectLugar(items.get(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
