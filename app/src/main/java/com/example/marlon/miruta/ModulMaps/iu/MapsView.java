package com.example.marlon.miruta.ModulMaps.iu;

import com.example.marlon.miruta.DataModel.Nodo;

import java.util.List;

public interface MapsView {
    void showLayoutMapa(boolean _boolean);
    void showMessageLayout(String _message);
    void showNodos(List _list);
    void showInfoMarket(Nodo _nodo);
    void showRutas(List _list);

}
