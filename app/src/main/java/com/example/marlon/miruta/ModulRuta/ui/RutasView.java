package com.example.marlon.miruta.ModulRuta.ui;

import java.util.List;

public interface RutasView {
    void showLayoutRuta(boolean _boolean);
    void showMessageLayout(String _message);
    void showRecyclerView(List _list);
    void showMessageOption(String _message,String selection);
    void showMessage(String _message);
}
