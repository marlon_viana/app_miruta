package com.example.marlon.miruta.ModulServicio;

import android.content.Context;
import android.util.Log;

import com.example.marlon.miruta.DataModel.Calificacion;
import com.example.marlon.miruta.DataModel.Comentario;
import com.example.marlon.miruta.DataModel.Message;
import com.example.marlon.miruta.ModulServicio.event.ServicioEvent;
import com.example.marlon.miruta.R;
import com.example.marlon.miruta.Services.PreferencesManager;
import com.example.marlon.miruta.Services.ServiceConectivity;
import com.example.marlon.miruta.Services.ServiceRequest;
import com.example.marlon.miruta.lib.base.EventBus;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServicioInteractorImpl implements  ServicioInteractor{
    private EventBus eventBus;
    private ServiceRequest request;
    private PreferencesManager preferences;
    private Context context;
    private String id_detalle,id_ruta;

    public ServicioInteractorImpl(EventBus eventBus, ServiceRequest request, PreferencesManager preferences, Context context) {
        this.eventBus= eventBus;
        this.request= request;
        this.preferences= preferences;
        this.context= context;
    }

    //Envia la califacion del usuario
    @Override
    public void sendCalificacion(String _iddetalle, String _idcalifacion, String _idruta) {
        if (ServiceConectivity.isNetDisponible(context)){

            request.calificacion(String.valueOf(preferences.getUser().getIDUSUARIOS()),
                    _iddetalle,_idcalifacion,_idruta).enqueue(new Callback<Message>() {
                @Override
                public void onResponse(Call<Message> call, Response<Message> response) {

                    Message message= response.body();
                    try {
                        messageEvent(message.getMessage(),ServicioEvent.showSnack);
                        getCalifiacionGeneral();
                    }catch (Exception e){

                    }
                }

                @Override
                public void onFailure(Call<Message> call, Throwable t) {
                    messageEvent(context.getResources().getString(R.string.califacion_error)
                            , ServicioEvent.showSnack);
                }
            });

        }else {
            messageEvent(context.getResources().getString(R.string.no_internet)
                    ,ServicioEvent.showSnack);
        }
    }

    //Envia el comentario del usuario
    @Override
    public void sendComentario(String _iddetalle, String _titulo, String _comentario) {
        if (ServiceConectivity.isNetDisponible(context)){

            request.comentario(String.valueOf(preferences.getUser().getIDUSUARIOS())
                    ,_iddetalle,_titulo,_comentario).enqueue(new Callback<Message>() {
                @Override
                public void onResponse(Call<Message> call, Response<Message> response) {

                    Message message= response.body();
                    try {
                        messageEvent(message.getMessage(),ServicioEvent.showSnack);
                        getComentarios();
                    }catch (Exception e){

                    }

                }

                @Override
                public void onFailure(Call<Message> call, Throwable t) {
                    messageEvent(context.getResources().getString(R.string.comantario_error),ServicioEvent.showSnack);
                }
            });

        }else {
            messageEvent(context.getResources().getString(R.string.no_internet)
                    ,ServicioEvent.showSnack);
        }
    }

    ///Obtiene los valores necesarios para obtener los comentariso y califaicon de un servicio
    @Override
    public void getValoracionComentarios(String _iddetalle, String _idruta) {
        id_detalle=_iddetalle;
        id_ruta=_idruta;
        getComentarios();
        getCalifiacionGeneral();
    }

    //Obtiene la califacion primedio de un servio
    public void getCalifiacionGeneral() {
        if (ServiceConectivity.isNetDisponible(context)){

            request.obtenercalifiacion(id_ruta,id_detalle).enqueue(new Callback<Calificacion>() {
                @Override
                public void onResponse(Call<Calificacion> call, Response<Calificacion> response) {
                    try {
                        Calificacion calificacion= response.body();
                        int valorpromedio= (int) Math.round(calificacion.getPromedio());
                        intEvent(valorpromedio,ServicioEvent.showRatingGeneral);
                    }catch (Exception e){
                        Log.e("ERROR","RESPUESTA CALIFICAICON GENERAL");
                    }
                }

                @Override
                public void onFailure(Call<Calificacion> call, Throwable t) {
                    messageEvent(context.getResources().getString(R.string.no_api)
                            ,ServicioEvent.showSnack);
                }
            });

        }else {
            messageEvent(context.getResources().getString(R.string.no_internet)
                    ,ServicioEvent.showSnack);
        }

    }

    //Obtiene los comentarios de un servicio
    public void getComentarios() {
        if (ServiceConectivity.isNetDisponible(context)){
                request.obtenercomentarios(id_detalle).enqueue(new Callback<List<Comentario>>() {
                    @Override
                    public void onResponse(Call<List<Comentario>> call, Response<List<Comentario>> response) {

                        try {
                            List <Comentario> comentarios= response.body();
                            messageEvent(preferences.getUser().getIDUSUARIOS().toString()
                                        ,ServicioEvent.getIdUser);
                            listEvent(comentarios,ServicioEvent.showComentarios);
                        }catch (Exception e){
                            messageEvent(context.getString(R.string.no_api)
                                    ,ServicioEvent.showSnack);
                            Log.e("ERROR COMENTARIOS",e.toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Comentario>> call, Throwable t) {
                        messageEvent(context.getString(R.string.no_api)
                                ,ServicioEvent.showSnack);
                    }
                });


        }else {
            messageEvent(context.getResources().getString(R.string.no_internet)
                    ,ServicioEvent.showSnack);
        }
    }

    //Eliminar comentario
    @Override
    public void deletComentario(String _idcomentario) {
        Log.e("ID COMENTARIO",_idcomentario);
        if (ServiceConectivity.isNetDisponible(context)){
           request.borrarComentario(_idcomentario).enqueue(new Callback<Message>() {
               @Override
               public void onResponse(Call<Message> call, Response<Message> response) {
                   try {
                       Message message= response.body();
                       messageEvent(message.getMessage(),ServicioEvent.showSnack);
                       getComentarios();
                   }catch (Exception e){
                       Log.e("ERROR COMENTARIO DELET",e.toString());
                       messageEvent(context.getString(R.string.error_borrar_comentario)
                               ,ServicioEvent.showSnack);
                   }

               }

               @Override
               public void onFailure(Call<Message> call, Throwable t) {
                   messageEvent(context.getString(R.string.error_borrar_comentario)
                           ,ServicioEvent.showSnack);
               }
           });


        }else {
            messageEvent(context.getString(R.string.no_internet)
                    ,ServicioEvent.showSnack);
        }

    }

    ///Eventos
    private void messageEvent(String message,int type){
        ServicioEvent event= new ServicioEvent();
        event.setEventType(type);
        event.setMessage(message);
        eventBus.postSticky(event);
    }

    private void listEvent(List list,int type){
        ServicioEvent event= new ServicioEvent();
        event.setEventType(type);
        event.setList(list);
        eventBus.postSticky(event);
    }

    private void intEvent(int i,int type){
        ServicioEvent event= new ServicioEvent();
        event.setEventType(type);
        event.setValorint(i);
        eventBus.postSticky(event);
    }

}
