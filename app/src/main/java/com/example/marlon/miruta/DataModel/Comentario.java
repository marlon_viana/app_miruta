package com.example.marlon.miruta.DataModel;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comentario {
    @SerializedName("ID_SERVICIOCOMENTARIO")
    @Expose
    private String iDSERVICIOCOMENTARIO;
    @SerializedName("ID_USUARIOS")
    @Expose
    private String iDUSUARIOS;
    @SerializedName("ID_DETALLE_LT_S")
    @Expose
    private String iDDETALLELTS;
    @SerializedName("USC_TITULO")
    @Expose
    private String uSCTITULO;
    @SerializedName("USC_COMENTARIO")
    @Expose
    private String uSCCOMENTARIO;
    @SerializedName("USC_FECHACOMENTARIO")
    @Expose
    private String uSCFECHACOMENTARIO;

    public String getIDSERVICIOCOMENTARIO() {
        return iDSERVICIOCOMENTARIO;
    }

    public void setIDSERVICIOCOMENTARIO(String iDSERVICIOCOMENTARIO) {
        this.iDSERVICIOCOMENTARIO = iDSERVICIOCOMENTARIO;
    }

    public String getIDUSUARIOS() {
        return iDUSUARIOS;
    }

    public void setIDUSUARIOS(String iDUSUARIOS) {
        this.iDUSUARIOS = iDUSUARIOS;
    }

    public String getIDDETALLELTS() {
        return iDDETALLELTS;
    }

    public void setIDDETALLELTS(String iDDETALLELTS) {
        this.iDDETALLELTS = iDDETALLELTS;
    }

    public String getUSCTITULO() {
        return uSCTITULO;
    }

    public void setUSCTITULO(String uSCTITULO) {
        this.uSCTITULO = uSCTITULO;
    }

    public String getUSCCOMENTARIO() {
        return uSCCOMENTARIO;
    }

    public void setUSCCOMENTARIO(String uSCCOMENTARIO) {
        this.uSCCOMENTARIO = uSCCOMENTARIO;
    }

    public String getUSCFECHACOMENTARIO() {
        return uSCFECHACOMENTARIO;
    }

    public void setUSCFECHACOMENTARIO(String uSCFECHACOMENTARIO) {
        this.uSCFECHACOMENTARIO = uSCFECHACOMENTARIO;
    }

    public String makeJson(){
        Gson gson= new Gson();
        String _jsonstring= gson.toJson(this);
        return  _jsonstring;
    }
}
