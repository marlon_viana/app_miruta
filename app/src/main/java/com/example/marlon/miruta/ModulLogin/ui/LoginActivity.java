package com.example.marlon.miruta.ModulLogin.ui;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marlon.miruta.MobileApplication;
import com.example.marlon.miruta.R;
import com.example.marlon.miruta.ModulLogin.LoginPressenter;
import com.example.marlon.miruta.ModulLogin.di.LoginComponent;
import com.example.marlon.miruta.ModulMain.ui.MainActivity;
import com.example.marlon.miruta.Services.Permission;

import javax.inject.Inject;

public class LoginActivity extends AppCompatActivity implements LoginView, View.OnClickListener {

    private EditText editTextEmail,editTextPass;
    private TextView textViewMensaje;
    private Button buttonLogin;
    private ConstraintLayout viewLogin,constraintLayoutLogin;

    @Inject
    LoginPressenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        addView();
        CheckPermissions();
        setupInject();
        presenter.onCreate();
    }

    private void setupInject() {
        MobileApplication application = (MobileApplication) getApplication();
        LoginComponent component = application.getLoginComponent(this);
        component.inject(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    private void addView(){
        constraintLayoutLogin= findViewById(R.id.constraintLayoutLogin);
        constraintLayoutLogin.setVisibility(View.GONE);

        viewLogin= findViewById(R.id.viewLogin);
        textViewMensaje= findViewById(R.id.textview_mensajelogin);
        editTextEmail= findViewById(R.id.editText_email);
        editTextPass=findViewById(R.id.editText_password);
        buttonLogin= findViewById(R.id.button_login);
        buttonLogin.setOnClickListener(this);
    }

    @Override
    public void validarLogin(boolean _boolean){
        if (_boolean){
            starActivityLogin();
        }else {
            constraintLayoutLogin.setVisibility(View.VISIBLE);
            LayoutAnimationController controller= AnimationUtils.loadLayoutAnimation
                    (constraintLayoutLogin.getContext(),R.anim.animlt_up);
            constraintLayoutLogin.setLayoutAnimation(controller);
            constraintLayoutLogin.scheduleLayoutAnimation();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_login:
                validateDate();
                break;
        }
    }


    @Override
    public void starActivityLogin() {
        Intent intentMain= new  Intent(getApplicationContext(), MainActivity.class);
        startActivity(intentMain);
        finish();
    }

    @Override
    public void showMessageLogin(String _mensaje) {
        textViewMensaje.setText(_mensaje);
    }

    @Override
    public void statusButton(Boolean _boolen) {

        if (_boolen){
            buttonLogin.setText(getResources().getString(R.string.ingresar));
        }else {
            buttonLogin.setText(getResources().getString(R.string.cargando));
        }

        buttonLogin.setClickable(_boolen);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void showSnackBar(String _mensaje) {
        Snackbar mSnackBar = Snackbar.make(viewLogin, _mensaje, Snackbar.LENGTH_LONG);
        TextView mainTextView =(mSnackBar.getView()).findViewById(android.support.design.R.id.snackbar_text);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            mainTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }else{
            mainTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        mainTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        mainTextView.setTextColor(getResources().getColor(R.color.colorblanco));
        mSnackBar.show();
    }

    private void validateDate(){
        String email=editTextEmail.getText().toString().trim();
        String pass=editTextPass.getText().toString().trim();

        if (email.isEmpty() || pass.isEmpty()){
            showMessageLogin(getResources().getString(R.string.error_campos));
        }else {
            showMessageLogin("");
            if(CheckPermissions()){
                presenter.sendLogin(email,pass);
            }
        }
    }

    //Comprobar los permisos
    public boolean CheckPermissions(){
        Boolean Retorno=true;
        Permission Permissions= new Permission(LoginActivity.this);

        if (!Permission.AceptGroupPermissionStorage()) {
            Permissions.requestPermissionForExternalStorage(Permissions.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_BY_LOAD_PROFILE);
            Retorno=false;
        }

        if(!Permission.checkPermissionForCamera()){
            Permissions.requestPermissionForCamera();
            Retorno=false;
        }

        return Retorno;
    }

    //Respuesta de los permisos
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case Permission.EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE_BY_LOAD_PROFILE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission granted successfully
                } else {
                    //permission denied
                    showSnackBar(getString(R.string.permiso_directorio));
                }
                CheckPermissions();
                break;
            case Permission.CAMERA_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission granted successfully
                } else {
                    //permission denied
                    showSnackBar(getString(R.string.permiso_camara));
                }
                break;
        }
    }
}
