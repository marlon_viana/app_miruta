package com.example.marlon.miruta.ModulRuta.ui;

import com.example.marlon.miruta.DataModel.Ruta;

public interface AdaptadorRutasListener {
    void selctRuta(Ruta _ruta);
    void deletRuta(Ruta _ruta);
    void shareRuta(Ruta _ruta);
}
