package com.example.marlon.miruta.ModulMaps.iu;

import com.example.marlon.miruta.DataModel.LugarTuristico;

public interface AdaptadorLugarListener {
    void selectLugar(LugarTuristico lugarTuristico);
}
