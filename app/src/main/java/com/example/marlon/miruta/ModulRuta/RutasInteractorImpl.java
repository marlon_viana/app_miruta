package com.example.marlon.miruta.ModulRuta;

import android.content.Context;
import android.util.Log;

import com.example.marlon.miruta.DataModel.Message;
import com.example.marlon.miruta.DataModel.Ruta;
import com.example.marlon.miruta.ModulRuta.event.RutasEvent;
import com.example.marlon.miruta.R;
import com.example.marlon.miruta.Services.PreferencesManager;
import com.example.marlon.miruta.Services.ServiceConectivity;
import com.example.marlon.miruta.Services.ServiceRequest;
import org.greenrobot.eventbus.EventBus;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RutasInteractorImpl implements RutasInteractor {

    private EventBus eventBus;
    private ServiceRequest request;
    private PreferencesManager preferences;
    private Context context;

    public RutasInteractorImpl(EventBus eventBus, ServiceRequest request, PreferencesManager preferences, Context context) {
        this.eventBus = eventBus;
        this.request = request;
        this.preferences = preferences;
        this.context= context;
    }


    ///La lista de rutas
    @Override
    public void obtenerRutas() {
        if (ServiceConectivity.isNetDisponible(context)){
            messageEvent(context.getResources().getString(R.string.cargando),RutasEvent.showMessageRuta);
            String iduser=preferences.getUser().getIDUSUARIOS().toString();
            request.listRutaUsuario(iduser).enqueue(new Callback<List<Ruta>>() {

                @Override
                public void onResponse(Call<List<Ruta>> call, Response<List<Ruta>> response){
                    try {

                        List<Ruta> listRutas= response.body();
                        if (listRutas.size()!=0){
                            boolenaEvent(false,RutasEvent.showLayoutRuta);
                        }else {
                            boolenaEvent(true,RutasEvent.showLayoutRuta);
                            messageEvent(context.getResources().getString(R.string.no_rutas)
                                    ,RutasEvent.showMessageRuta);
                        }

                        listEvent(listRutas,RutasEvent.showRecyclerView);

                    }catch (Exception e){
                        Log.e("ERROR RUTA RESPUESTA",e.toString());
                    }
                }
                @Override
                public void onFailure(Call<List<Ruta>> call, Throwable t) {
                    boolenaEvent(true,RutasEvent.showLayoutRuta);
                    messageEvent(context.getResources().getString(R.string.no_api)
                                ,RutasEvent.showMessageRuta);
                }
            });

        }else {
            boolenaEvent(true,RutasEvent.showLayoutRuta);
            messageEvent(context.getResources().getString(R.string.no_internet)
                        ,RutasEvent.showMessageRuta);
        }
    }

    //Eliminar ruta
    @Override
    public void borrarRuta(String _idRuta) {
        if (ServiceConectivity.isNetDisponible(context)){
            request.borrarRuta(preferences.getUser().getIDUSUARIOS().toString(),_idRuta).enqueue(new Callback<Message>() {
                @Override
                public void onResponse(Call<Message> call, Response<Message> response) {
                    try {
                        Message message= response.body();
                        messageEvent(message.getMessage(),RutasEvent.showMessage);
                        obtenerRutas();
                    }catch (Exception e){
                        Log.e("MESSAGe",e.toString());
                    }
                }

                @Override
                public void onFailure(Call<Message> call, Throwable t) {
                    messageEvent(context.getString(R.string.error_borrar_ruta),RutasEvent.showMessage);
                }
            });

        }else {
            boolenaEvent(true,RutasEvent.showLayoutRuta);
            messageEvent(context.getString(R.string.no_internet)
                    ,RutasEvent.showMessage);
        }

    }

    ///Eventos
    private void messageEvent(String message,int type){
        RutasEvent event= new RutasEvent();
        event.setEventType(type);
        event.setMessage(message);
        eventBus.postSticky(event);
    }

    private void boolenaEvent(Boolean _boolean,int type){
        RutasEvent event= new RutasEvent();
        event.setEventType(type);
        event.setaBoolean(_boolean);
        eventBus.postSticky(event);
    }

    private void listEvent(List list,int type){
        RutasEvent event= new RutasEvent();
        event.setEventType(type);
        event.setList(list);
        eventBus.postSticky(event);
    }


}
