package com.example.marlon.miruta.DataModel;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ruta{
    @SerializedName("ID_RUTA")
    @Expose
    private String iDRUTA;
    @SerializedName("RT_NOMBRE")
    @Expose
    private String rTNOMBRE;
    @SerializedName("RT_DESCRIPCION")
    @Expose
    private String rTDESCRIPCION;
    @SerializedName("RT_UBICACION_INICIO_LATITUD")
    @Expose
    private Object rTUBICACIONINICIOLATITUD;
    @SerializedName("RT_UBICACION_INICIO_LONGITUD")
    @Expose
    private Object rTUBICACIONINICIOLONGITUD;
    @SerializedName("RT_UBICACION_FINAL_LATITUD")
    @Expose
    private Object rTUBICACIONFINALLATITUD;
    @SerializedName("RT_UBICACION_FINAL_LONTITUD")
    @Expose
    private Object rTUBICACIONFINALLONTITUD;

    public String getIDRUTA() {
        return iDRUTA;
    }

    public void setIDRUTA(String iDRUTA) {
        this.iDRUTA = iDRUTA;
    }

    public String getRTNOMBRE() {
        return rTNOMBRE;
    }

    public void setRTNOMBRE(String rTNOMBRE) {
        this.rTNOMBRE = rTNOMBRE;
    }

    public String getRTDESCRIPCION() {
        return rTDESCRIPCION;
    }

    public void setRTDESCRIPCION(String rTDESCRIPCION) {
        this.rTDESCRIPCION = rTDESCRIPCION;
    }

    public Object getRTUBICACIONINICIOLATITUD() {
        return rTUBICACIONINICIOLATITUD;
    }

    public void setRTUBICACIONINICIOLATITUD(Object rTUBICACIONINICIOLATITUD) {
        this.rTUBICACIONINICIOLATITUD = rTUBICACIONINICIOLATITUD;
    }

    public Object getRTUBICACIONINICIOLONGITUD() {
        return rTUBICACIONINICIOLONGITUD;
    }

    public void setRTUBICACIONINICIOLONGITUD(Object rTUBICACIONINICIOLONGITUD) {
        this.rTUBICACIONINICIOLONGITUD = rTUBICACIONINICIOLONGITUD;
    }

    public Object getRTUBICACIONFINALLATITUD() {
        return rTUBICACIONFINALLATITUD;
    }

    public void setRTUBICACIONFINALLATITUD(Object rTUBICACIONFINALLATITUD) {
        this.rTUBICACIONFINALLATITUD = rTUBICACIONFINALLATITUD;
    }

    public Object getRTUBICACIONFINALLONTITUD() {
        return rTUBICACIONFINALLONTITUD;
    }

    public void setRTUBICACIONFINALLONTITUD(Object rTUBICACIONFINALLONTITUD) {
        this.rTUBICACIONFINALLONTITUD = rTUBICACIONFINALLONTITUD;
    }

    public String makeJson(){
        Gson gson= new Gson();
        String _jsonstring= gson.toJson(this);
        return  _jsonstring;
    }
}
